package ir.madtalk.android.rollcall.rollCall;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.os.Bundle;

import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItemAdapter;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItems;

import ir.madtalk.android.rollcall.rollCall.fragments.AbsentListFragment;
import ir.madtalk.android.rollcall.rollCall.fragments.RollCallListFragment;
import ir.madtalk.android.rollcall.R;
import ir.madtalk.android.rollcall.databinding.ActivityRollCallBinding;

public class RollCallActivity extends AppCompatActivity {

    ActivityRollCallBinding binding;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this , R.layout.activity_roll_call);

        FragmentPagerItemAdapter adapter = new FragmentPagerItemAdapter(
                getSupportFragmentManager(), FragmentPagerItems.with(this)
                .add(getString(R.string.apsentList), AbsentListFragment.class)
                .add(getString(R.string.rollCallList), RollCallListFragment.class)
                .create());

         binding.viewpager.setAdapter(adapter);
         binding.viewpagertab.setViewPager(binding.viewpager);

    }
}