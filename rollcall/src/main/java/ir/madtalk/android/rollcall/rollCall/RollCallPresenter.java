package ir.madtalk.android.rollcall.rollCall;

import androidx.recyclerview.widget.LinearLayoutManager;

import java.lang.reflect.Array;
import java.util.ArrayList;

import ir.madtalk.android.rollcall.module.AbsentItem;
import ir.madtalk.android.rollcall.module.RollCallItem;
import ir.madtalk.android.rollcall.rollCall.adapter.AbsentListAdapter;
import ir.madtalk.android.rollcall.rollCall.fragments.AbsentListFragment;
import ir.madtalk.android.rollcall.rollCall.fragments.RollCallListFragment;

public class RollCallPresenter {
   private static RollCallPresenter rollCallPresenter ;

    public static RollCallPresenter getInstance() {
        if (rollCallPresenter == null){
            rollCallPresenter = new RollCallPresenter();
        }
        return rollCallPresenter;
    }

    public void getItems(AbsentListFragment fragment){
        ArrayList<AbsentItem> items =new ArrayList<>();
        items.add(new AbsentItem());
        items.add(new AbsentItem());
        items.add(new AbsentItem());
        items.add(new AbsentItem());
        items.add(new AbsentItem());
        items.add(new AbsentItem());

        fragment.adapter.versionModels.accept(items);

    }
    
    public void getItem_rollCall(RollCallListFragment fragment){
        ArrayList<RollCallItem> items =new ArrayList<>();

        items.add(new RollCallItem());
        items.add(new RollCallItem());
        items.add(new RollCallItem());
        items.add(new RollCallItem());
        items.add(new RollCallItem());
        items.add(new RollCallItem());

        fragment.adapter.versionModels.accept(items);
        
    }

}
