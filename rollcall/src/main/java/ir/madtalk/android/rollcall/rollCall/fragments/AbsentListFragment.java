package ir.madtalk.android.rollcall.rollCall.fragments;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;

import java.util.ArrayList;

import ir.madtalk.android.rollcall.module.AbsentItem;
import ir.madtalk.android.rollcall.rollCall.RollCallPresenter;
import ir.madtalk.android.rollcall.rollCall.adapter.AbsentListAdapter;
import ir.madtalk.android.rollcall.R;
import ir.madtalk.android.rollcall.databinding.ApsentListBinding;

public class AbsentListFragment extends Fragment {


    ApsentListBinding binding;
    AbsentListAdapter absentListAdapter;
    public AbsentListAdapter adapter ;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.apsent_list, container, false);
        Log.e("fragment","oncreateView");
        View view = binding.getRoot();

        //region initial recycle
        adapter = new AbsentListAdapter(this);
        binding.apsentListRecycler.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        binding.apsentListRecycler.setAdapter(adapter);
        //endregion

        RollCallPresenter.getInstance().getItems(this);


        return view;
    }
}
