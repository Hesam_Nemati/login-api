package ir.madtalk.android.rollcall.rollCall.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.recyclerview.widget.RecyclerView;

import com.jakewharton.rxrelay2.BehaviorRelay;

import java.util.ArrayList;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import ir.madtalk.android.rollcall.R;
import ir.madtalk.android.rollcall.databinding.AbsentBaseBinding;
import ir.madtalk.android.rollcall.module.AbsentItem;
import ir.madtalk.android.rollcall.rollCall.fragments.AbsentListFragment;

public class AbsentListAdapter extends  RecyclerView.Adapter<AbsentListAdapter.VersionViewHolder> {
    AbsentListFragment context;


    public AbsentListAdapter(AbsentListFragment context) {
        this.context = context;

    }

    public BehaviorRelay<ArrayList<AbsentItem>> versionModels = BehaviorRelay.createDefault(new ArrayList<>());

    {
        versionModels.observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<ArrayList<AbsentItem>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                    }

                    @Override
                    public void onNext(ArrayList<AbsentItem> absentItems) {

                        notifyDataSetChanged();
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }


    @Override
    public VersionViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.absent_base, parent, false);
        AbsentListAdapter.VersionViewHolder viewHolder = new AbsentListAdapter.VersionViewHolder(view);

        return viewHolder;
    }



    @Override
    public void onBindViewHolder(final VersionViewHolder holder, final int position) {

        //position -> current item



    }

    @Override
    public int getItemCount() {
        return versionModels.getValue() == null ? 0 : versionModels.getValue().size();
    }

    public class VersionViewHolder extends RecyclerView.ViewHolder {

        AbsentBaseBinding binding;

        public VersionViewHolder(View itemView) {
            super(itemView);
            binding = AbsentBaseBinding.bind(itemView);
        }
    }
}



