package ir.madtalk.android.rollcall.rollCall.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.recyclerview.widget.RecyclerView;

import com.jakewharton.rxrelay2.BehaviorRelay;

import java.util.ArrayList;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import ir.madtalk.android.rollcall.R;
import ir.madtalk.android.rollcall.databinding.RollCallBaseBinding;
import ir.madtalk.android.rollcall.module.RollCallItem;
import ir.madtalk.android.rollcall.rollCall.fragments.RollCallListFragment;

public class RollCallListAdapter extends  RecyclerView.Adapter<RollCallListAdapter.VersionViewHolder> {
    RollCallListFragment context;


    public RollCallListAdapter(RollCallListFragment context) {
        this.context = context;
    }

   public BehaviorRelay<ArrayList<RollCallItem>> versionModels = BehaviorRelay.createDefault(new ArrayList<>());

    {
        versionModels.observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<ArrayList<RollCallItem>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                    }

                    @Override
                    public void onNext(ArrayList<RollCallItem> rollCallItems) {

                        notifyDataSetChanged();
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }


    @Override
    public RollCallListAdapter.VersionViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.roll_call_base, parent, false);
        RollCallListAdapter.VersionViewHolder viewHolder = new RollCallListAdapter.VersionViewHolder(view);

        return viewHolder;
    }



    @Override
    public void onBindViewHolder(final RollCallListAdapter.VersionViewHolder holder, final int position) {




    }

    @Override
    public int getItemCount() {
        return versionModels.getValue() == null ? 0 : versionModels.getValue().size();
    }

    public class VersionViewHolder extends RecyclerView.ViewHolder {

        RollCallBaseBinding binding;

        public VersionViewHolder(View itemView) {
            super(itemView);
            binding = RollCallBaseBinding.bind(itemView);
        }
    }
}



