package ir.madtalk.android.rollcall.rollCall.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;

import ir.madtalk.android.rollcall.rollCall.RollCallPresenter;
import ir.madtalk.android.rollcall.rollCall.adapter.RollCallListAdapter;
import ir.madtalk.android.rollcall.R;
import ir.madtalk.android.rollcall.databinding.RollCallListBinding;

public class RollCallListFragment extends Fragment {

    RollCallListBinding binding;
   public RollCallListAdapter adapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.roll_call_list, container, false);
        View view = binding.getRoot();

        adapter = new RollCallListAdapter(this);
        binding.rollCallListRecycler.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        binding.rollCallListRecycler.setAdapter(adapter);

        RollCallPresenter.getInstance().getItem_rollCall(this);

        return view;
    }
}
