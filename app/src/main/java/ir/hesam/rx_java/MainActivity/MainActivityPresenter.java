package ir.hesam.rx_java.MainActivity;

import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.sothree.slidinguppanel.SlidingUpPanelLayout;

import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import ir.hesam.rx_java.G;
import ir.hesam.rx_java.Home.HomeActivity;
import ir.hesam.rx_java.NetWork.API.NetworkLayer;
import ir.hesam.rx_java.module.UserLogin;

public class MainActivityPresenter {

    private static final MainActivityPresenter ourInstance = new MainActivityPresenter();
    public static MainActivityPresenter getInstance(){ return ourInstance;}

    public MainActivityPresenter() {
    }

    public void setSlidingPanel(MainActivity activity){

        activity.binding.slidablePanel.setPanelState(SlidingUpPanelLayout.PanelState.HIDDEN);

        activity.binding.loginPhone.setOnClickListener(view -> {
            activity.binding.slidablePanel.setPanelState(SlidingUpPanelLayout.PanelState.HIDDEN);
        });

        activity.binding.loginPhonee.setOnClickListener(view -> {
            activity.binding.slidablePanel.setPanelState(SlidingUpPanelLayout.PanelState.EXPANDED);
        });

    }


    public void Login_UserName(MainActivity activity , String userName , String passWord ){
//        , String first_name , String last_name, String type, String username

        NetworkLayer.getInstance().login(userName,passWord)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<UserLogin>() {



                    @Override
                    public void onSubscribe(@NonNull Disposable d) {

                    }

                    @Override
                    public void onSuccess(@NonNull UserLogin login) {


                        G.add_to_sharedPref(login.getAccess() , login.getUser().getName() , login.getUser().getType() , login.getUser().getUsername());
                        Intent i = new Intent(activity, HomeActivity.class);
                        activity.startActivity(i);

                        Log.e("token" , login.getAccess());
                        Log.e("name" , login.getUser().getName());
                        Log.e("user" , login.getUser().getUsername());


                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        Toast.makeText(activity, "onError" , Toast.LENGTH_LONG).show();


                    }
                });

    }
}
