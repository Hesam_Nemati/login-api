package ir.hesam.rx_java.MainActivity;

import android.os.Bundle;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.sothree.slidinguppanel.SlidingUpPanelLayout;

import ir.hesam.rx_java.NetWork.API.APIClient;
import ir.hesam.rx_java.R;
import ir.hesam.rx_java.databinding.ActivityMainBinding;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;


public class MainActivity extends AppCompatActivity {

    ActivityMainBinding binding;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        
        login_btn();

    }




    @Override
    public void onBackPressed() {
        if (binding.slidablePanel != null &&
                binding.slidablePanel.getPanelState() == SlidingUpPanelLayout.PanelState.EXPANDED
                || binding.slidablePanel.getPanelState() == SlidingUpPanelLayout.PanelState.ANCHORED
                || binding.slidablePanel.getPanelState() == SlidingUpPanelLayout.PanelState.COLLAPSED
                 ){
            binding.slidablePanel.setPanelState(SlidingUpPanelLayout.PanelState.HIDDEN);
        }
        else {
            super.onBackPressed();
        }
    }

    public void login_btn(){
        binding.loginUserBtn.setOnClickListener(view -> {

            MainActivityPresenter.getInstance().Login_UserName(this , binding.userNameEt.getText().toString() , binding.passwordEt.getText().toString());

        });
    }
}



