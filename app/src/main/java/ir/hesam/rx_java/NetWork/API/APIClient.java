package ir.hesam.rx_java.NetWork.API;

import io.reactivex.Single;
import ir.hesam.rx_java.module.Class_Online_List;
import ir.hesam.rx_java.module.FileResponse;
import ir.hesam.rx_java.module.JoinClass;
import ir.hesam.rx_java.module.RecordedClassResult;
import ir.hesam.rx_java.module.ReponseBody.AnnounceList;
import ir.hesam.rx_java.module.UserLogin;
import okhttp3.MultipartBody;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;

public interface APIClient {

    //region login with username

    @FormUrlEncoded
    @POST("v1.0.0/auth/login/base/")
    Single<UserLogin> login(@Field("username") String username, @Field("password") String password);


    @GET("v1.0.0/announcement/")
    Single<AnnounceList> get_announce(@Header("Authorization") String token);


    @Multipart
    @POST("v1.0.0/file/upload/")
    Single<FileResponse> new_file(@Header("Authorization") String token, @Part MultipartBody.Part file);


    @GET("v1.0.0/online-class/list/")
    Single<Class_Online_List> get_classOnline(@Header("Authorization") String token);


    @GET("v1.0.0/online-class/join/{id}/")
    Single<JoinClass> join_online_class(@Header("Authorization") String token, @Path("id") int id);


    @GET("v1.0.0/online-class/record/list/{id}/")
    Single<RecordedClassResult> go_to_recoredeFile(@Header("Authorization") String token , @Path("id") int id);

    //endregion
}
