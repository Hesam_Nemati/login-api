package ir.hesam.rx_java.NetWork.API;

import io.reactivex.Single;
import ir.hesam.rx_java.module.Class_Online_List;
import ir.hesam.rx_java.module.FileResponse;
import ir.hesam.rx_java.module.JoinClass;
import ir.hesam.rx_java.module.RecordedClassResult;
import ir.hesam.rx_java.module.ReponseBody.AnnounceList;
import ir.hesam.rx_java.module.UserLogin;
import okhttp3.MultipartBody;

public class NetworkLayer implements APIClient {
    private APIClient apiClient;
    private static final NetworkLayer ourInstance = new NetworkLayer();

    public static NetworkLayer getInstance() {
        return ourInstance;
    }

    public NetworkLayer() {
        apiClient = ApiService.getApiClient();
    }

    @Override
    public Single<UserLogin> login(String username, String password) {
        return apiClient.login(username, password);
    }

    @Override
    public Single<AnnounceList> get_announce(String token) {
        return apiClient.get_announce(token);
    }

    @Override
    public Single<FileResponse> new_file(String token, MultipartBody.Part file) {
        return apiClient.new_file(token, file);
    }

    @Override
    public Single<Class_Online_List> get_classOnline(String token) {
        return apiClient.get_classOnline(token);
    }

    @Override
    public Single<JoinClass> join_online_class(String token , int id ) {
        return apiClient.join_online_class(token , id );
    }

    public Single<RecordedClassResult> go_to_recoredeFile (String token , int id){
        return apiClient.go_to_recoredeFile(token , id);
    }

}