package ir.hesam.rx_java.DaynamicOnlin;

import static android.view.ViewGroup.LayoutParams.MATCH_PARENT;
import static android.view.ViewGroup.LayoutParams.WRAP_CONTENT;

import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.appbar.MaterialToolbar;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.android.material.textview.MaterialTextView;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;

import ir.hesam.rx_java.R;

public class classOnline extends AppCompatActivity {

//    RelativeLayout mainLayout;
//    CardView cardView;
//    LinearLayout linearLayout;
//    LinearLayout linearLayoutHorizontal;
//    LinearLayout lineHorizontal;
//    MaterialTextView tittle;
//    MaterialTextView text;
//    ImageView nullImage;
//    ImageView line;
//    ImageView lineImage;
//    ImageView nullImageLine;
//    LinearLayout buttonsLinear;
//    Button enterBtn;
//    Button recordesBtn;
//
//    RelativeLayout mainLayoutR;
//    MaterialToolbar toolbar;
//    TextInputLayout name;
//    TextInputEditText nameEt;
//    TextInputLayout lesson;
//    TextInputEditText lessonBtn;
//    Button button;
//    MaterialButton confirmBtn;
//    SlidingUpPanelLayout sliding;
//
//    RelativeLayout main;
//    RecyclerView recyclerView;
//    ExtendedFloatingActionButton addBtn;

    CardView cardView;
    MaterialTextView holdingTime;
    MaterialTextView time;
    MaterialTextView showDetail;
    RelativeLayout mainLayout;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        cardView = new CardView(this);
        holdingTime = new MaterialTextView(this);
        time = new MaterialTextView(this);
        mainLayout = new RelativeLayout(this);
        showDetail = new MaterialTextView(this);


        RelativeLayout.LayoutParams cardViewParam = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT , ViewGroup.LayoutParams.WRAP_CONTENT);
        cardViewParam.setLayoutDirection(RelativeLayout.LAYOUT_DIRECTION_RTL);
        cardViewParam.setMargins(20,10,20,10);
        cardView.setRadius(12);
        cardView.setBackground(getResources().getDrawable(R.drawable.green_a));

        cardView.setLayoutParams(cardViewParam);
        cardView.setId(View.generateViewId());

        RelativeLayout.LayoutParams mainLayoutParam = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT , ViewGroup.LayoutParams.MATCH_PARENT);
        mainLayoutParam.setLayoutDirection(RelativeLayout.LAYOUT_DIRECTION_RTL);
        mainLayoutParam.setMargins(30 , 10 , 30 , 10);
        mainLayout.setLayoutParams(mainLayoutParam);
        mainLayout.setId(View.generateViewId());

        RelativeLayout.LayoutParams holdingTimeParam = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT , ViewGroup.LayoutParams.WRAP_CONTENT);
        holdingTimeParam.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
        holdingTimeParam.setLayoutDirection(RelativeLayout.LAYOUT_DIRECTION_RTL);
        holdingTime.setLayoutParams(holdingTimeParam);
        holdingTime.setId(View.generateViewId());
        holdingTime.setTextColor(getResources().getColor(R.color.black));
        holdingTime.setText("زمان برگذاری");

        RelativeLayout.LayoutParams timeParam = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT , ViewGroup.LayoutParams.WRAP_CONTENT);
        time.setLayoutParams(timeParam);
        timeParam.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
        time.setId(View.generateViewId());
        time.setText("00:00:00:00");

        RelativeLayout.LayoutParams showDetailParam = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT , ViewGroup.LayoutParams.WRAP_CONTENT);
        showDetail.setLayoutParams(showDetailParam);
        showDetailParam.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
        showDetailParam.addRule(RelativeLayout.BELOW,time.getId());
        showDetail.setId(View.generateViewId());
        showDetail.setTextColor(getResources().getColor(R.color.green_font));
        showDetail.setCompoundDrawablesWithIntrinsicBounds(R.drawable.left_arrow_path , 0, 0, 0);
        showDetail.setText("مشاهده گزارش");


        mainLayout.addView(holdingTime , holdingTimeParam);
        mainLayout.addView(time , timeParam);
        mainLayout.addView(showDetail , showDetailParam);
        cardView.addView(mainLayout , mainLayoutParam);
        setContentView(cardView);






















//
//        mainLayout = new RelativeLayout(this);
//        cardView = new CardView(this);
//        linearLayout = new LinearLayout(this);
//        linearLayoutHorizontal = new LinearLayout(this);
//        tittle = new MaterialTextView(this);
//        text = new MaterialTextView(this);
//        nullImage = new ImageView(this);
//        line = new ImageView(this);
//        lineHorizontal = new LinearLayout(this);
//        nullImageLine = new ImageView(this);
//        buttonsLinear = new LinearLayout(this);
//        enterBtn = new Button(this);
//        recordesBtn =new Button(this);
//
////
//
//        mainLayoutR = new RelativeLayout(this);
//        nameEt = new TextInputEditText(this);
//        name = new TextInputLayout(this);
//        lessonBtn = new TextInputEditText(this);
//        confirmBtn = new MaterialButton(this);
//        lesson =new TextInputLayout(this);
//        sliding = new SlidingUpPanelLayout(this);
//        button = new Button(this);
//        toolbar = new MaterialToolbar(this);
//
//        main = new RelativeLayout(this);
//        recyclerView = new RecyclerView(this);
//        addBtn = new ExtendedFloatingActionButton(this);
//
//
//        RelativeLayout.LayoutParams slidingParam = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
//        sliding.setLayoutParams(slidingParam);
//        sliding.setGravity(Gravity.BOTTOM);
////        sliding.setOverlayed(true);
//        sliding.setPanelHeight(0);
//
//
//        RelativeLayout.LayoutParams mainLayoutRParam = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT , ViewGroup.LayoutParams.WRAP_CONTENT);
//        mainLayoutR.setLayoutParams(mainLayoutRParam);
//
//        //toolbar Dynamic
//
//        RelativeLayout.LayoutParams nameParam = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT , ViewGroup.LayoutParams.WRAP_CONTENT);
//        name.setLayoutParams(nameParam);
//        nameParam.addRule(RelativeLayout.ALIGN_PARENT_TOP);
//        name.setId(View.generateViewId());
//        nameParam.setMargins(30,10,30,20);
//        name.setHint("نام جلسه");
//        name.setBoxBackgroundColor(getResources().getColor(R.color.white));
//
//
//
////        RelativeLayout.LayoutParams nameEtParam = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT , ViewGroup.LayoutParams.WRAP_CONTENT);
////        nameEt.setLayoutParams(nameEtParam);
//        nameEt.setGravity(Gravity.RIGHT);
//        nameEt.setId(View.generateViewId());
//        nameEt.setBackgroundResource(android.R.color.transparent);
//        nameEt.setPadding(0,90,0,0);
//        nameEt.setTextColor(getResources().getColor(R.color.black));
//
//
//
//        RelativeLayout.LayoutParams lessonParam = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
//        lesson.setLayoutParams(lessonParam);
//        lesson.setId(View.generateViewId());
//        lessonParam.setMargins(30,10,30,20);
//        lessonParam.addRule(RelativeLayout.BELOW, name.getId());
//        lessonBtn.setHint("درس");
//        lesson.setStartIconDrawable(getResources().getDrawable(R.drawable.cheque_icon));
//
////        RelativeLayout.LayoutParams lessonBtnParam = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT , ViewGroup.LayoutParams.WRAP_CONTENT);
////        lessonBtn.setLayoutParams(lessonBtnParam);
//        lessonBtn.setId(View.generateViewId());
//        lessonBtn.setBackgroundResource(android.R.color.transparent);
//
//
//
//        RelativeLayout.LayoutParams buttonParam = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
//        button.setLayoutParams(buttonParam);
//        buttonParam.addRule(RelativeLayout.BELOW,name.getId());
//        button.setBackground(null);
//
//
//
//        RelativeLayout.LayoutParams confirmBtnParam = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT , ViewGroup.LayoutParams.WRAP_CONTENT);
//        confirmBtn.setLayoutParams(confirmBtnParam);
//        confirmBtnParam.addRule(RelativeLayout.BELOW , lesson.getId());
//        confirmBtnParam.setMargins(0,50,0,50);
//        confirmBtn.setText("ثبت");
//        confirmBtnParam.addRule(RelativeLayout.CENTER_HORIZONTAL);
//
//
//        RelativeLayout.LayoutParams mainParam = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT , ViewGroup.LayoutParams.MATCH_PARENT);
//        main.setLayoutParams(mainParam);
//
//
//
//        RelativeLayout.LayoutParams recycleParam = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT , ViewGroup.LayoutParams.WRAP_CONTENT);
//        recyclerView.setLayoutParams(recycleParam);
//
//        RelativeLayout.LayoutParams btnParams = new RelativeLayout.LayoutParams(WRAP_CONTENT, WRAP_CONTENT);
//        btnParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
//        btnParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
//        btnParams.setMargins(0, 0,10,10);
//        addBtn.setText("افزودن");
//        addBtn.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
//        addBtn.setIcon(getResources().getDrawable(R.drawable.ic_action_add_green));
//        addBtn.setId(View.generateViewId());
//        addBtn.setLayoutParams(btnParams);
//        addBtn.setTextColor(getResources().getColor(R.color.white));
//
//
//
//        addBtn.setOnClickListener(view -> {
//            sliding.setPanelState(SlidingUpPanelLayout.PanelState.EXPANDED);
//        });
//
//
//        main.addView(addBtn,btnParams);
//        main.addView(recyclerView ,recycleParam);
////        setContentView(main);
//
//
////        name.addView(nameEt,nameEtParam);
//        mainLayoutR.addView(button,buttonParam);
//        name.addView(nameEt);
//        mainLayoutR.addView(name , nameParam);
//        lesson.addView(lessonBtn);
//        mainLayoutR.addView(lesson, lessonParam);
//        mainLayoutR.addView(confirmBtn , confirmBtnParam);
//        sliding.addView(main,mainParam);
//        sliding.addView(mainLayoutR,mainLayoutRParam);
//        setContentView(sliding);











//        RelativeLayout.LayoutParams mainLayoutParam = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
//        mainLayout.setLayoutParams(mainLayoutParam);
//
//
//        RelativeLayout.LayoutParams cardViewParam = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
//        cardView.setLayoutParams(cardViewParam);
//        cardViewParam.setMargins(30,30,30,30);
//        cardView.setRadius(15);
//
//        RelativeLayout.LayoutParams linearLayoutParam = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
//        linearLayout.setLayoutParams(linearLayoutParam);
//        linearLayout.setOrientation(LinearLayout.VERTICAL);
//        linearLayout.setGravity(Gravity.RIGHT);
//
//        LinearLayout.LayoutParams horizontalLinearParam = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
//        linearLayoutHorizontal.setLayoutParams(horizontalLinearParam);
//        linearLayoutHorizontal.setOrientation(LinearLayout.HORIZONTAL);
//        linearLayoutHorizontal.setWeightSum(16);
//
//        LinearLayout.LayoutParams tittleParam = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT);
//        tittle.setLayoutParams(tittleParam);
//        tittle.setText("شیمی۱");
//        tittleParam.gravity = Gravity.RIGHT;
//        tittle.setGravity(Gravity.RIGHT);
//        tittleParam.rightMargin = 12;
//        tittleParam.weight = 12;
//
//        LinearLayout.LayoutParams nullImageParam = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT);
//        nullImage.setLayoutParams(nullImageParam);
//        nullImageParam.weight = 4;
//
//        LinearLayout.LayoutParams lineLayoutParam = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT , ViewGroup.LayoutParams.WRAP_CONTENT);
//        lineHorizontal.setLayoutParams(lineLayoutParam);
//        lineHorizontal.setWeightSum(16);
//
//        LinearLayout.LayoutParams lineParam = new LinearLayout.LayoutParams(0 , 1);
//        line.setLayoutParams(lineParam);
//        line.setBackgroundColor(getResources().getColor(ir.madtalk.android.cheque.R.color.black));
//        lineParam.leftMargin = 15 ;
//        lineParam.weight = 15;
//
//        LinearLayout.LayoutParams nullImageLineParam = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT);
//        nullImageLine.setLayoutParams(nullImageLineParam);
//        nullImageLineParam.weight = 1;
//
//
//        LinearLayout.LayoutParams textParam = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT , ViewGroup.LayoutParams.WRAP_CONTENT);
//        text.setLayoutParams(textParam);
//        text.setText("jdgpijdfgopidj");
//        textParam.setMargins(12, 30 , 30 , 30);
//        text.setTextColor(getResources().getColor(ir.madtalk.android.cheque.R.color.black));
//        text.setTextSize(18);
//
//        LinearLayout.LayoutParams buttonsLinearParam = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT , ViewGroup.LayoutParams.WRAP_CONTENT);
//        buttonsLinear.setLayoutParams(buttonsLinearParam);
//        buttonsLinear.setOrientation(LinearLayout.HORIZONTAL);
//        buttonsLinear.setWeightSum(2);
//
//        LinearLayout.LayoutParams enterBtnParam = new LinearLayout.LayoutParams(0 ,ViewGroup.LayoutParams.WRAP_CONTENT);
//        enterBtn.setLayoutParams(enterBtnParam);
//        enterBtn.setText("ورود");
//        enterBtn.setBackgroundColor(getResources().getColor(ir.madtalk.android.cheque.R.color.green_font));
//        enterBtn.setTextColor(getResources().getColor(ir.madtalk.android.cheque.R.color.white));
//        enterBtnParam.leftMargin = 2;
//        enterBtnParam.weight = 1;
//
//
//        LinearLayout.LayoutParams recordBtnParam = new LinearLayout.LayoutParams(0 ,ViewGroup.LayoutParams.WRAP_CONTENT);
//        recordesBtn.setLayoutParams(recordBtnParam);
//        recordesBtn.setText("ضبط ها");
//        recordesBtn.setBackgroundColor(getResources().getColor(ir.madtalk.android.cheque.R.color.black));
//        recordesBtn.setTextColor(getResources().getColor(ir.madtalk.android.cheque.R.color.white));
//        recordBtnParam.rightMargin = 2 ;
//        recordBtnParam.weight = 1;


//        linearLayoutHorizontal.addView(nullImage, nullImageParam);
//        linearLayoutHorizontal.addView(tittle, tittleParam);
//        linearLayout.addView(linearLayoutHorizontal, horizontalLinearParam);
//        linearLayout.addView(lineHorizontal , lineLayoutParam);
//        lineHorizontal.addView(line , lineParam);
//        lineHorizontal.addView(nullImageLine , nullImageLineParam);
//        linearLayout.addView(text , textParam);
//        buttonsLinear.addView(recordesBtn , recordBtnParam);
//        buttonsLinear.addView(enterBtn , enterBtnParam);
//        linearLayout.addView(buttonsLinear , buttonsLinearParam);
//        cardView.addView(linearLayout, linearLayoutParam);
//        mainLayout.addView(cardView, cardViewParam);
//        setContentView(mainLayout);





    }
}
