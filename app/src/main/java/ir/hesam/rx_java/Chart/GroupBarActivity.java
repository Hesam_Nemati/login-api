package ir.hesam.rx_java.Chart;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.adapters.RadioGroupBindingAdapter;

import android.os.Bundle;

import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter;

import java.util.ArrayList;

import ir.hesam.rx_java.R;
import ir.hesam.rx_java.databinding.ActivityGroupBarBinding;

public class GroupBarActivity extends AppCompatActivity {

    ActivityGroupBarBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this ,R.layout.activity_group_bar);

        BarDataSet barDataSet1 = new BarDataSet(barEntries1(), "Data set 1");
        barDataSet1.setColor(getResources().getColor(R.color.red));

        BarDataSet barDataSet2 = new BarDataSet(barEntries2() , "data set 2");
        barDataSet2.setColor(getResources().getColor(R.color.teal_200));

        BarDataSet barDataSet3 = new BarDataSet(barEntries3() , "data set 3");
        barDataSet3.setColor(getResources().getColor(R.color.teal_700));

        BarDataSet barDataSet4 = new BarDataSet(barEntries4() , "data set 4");
        barDataSet4.setColor(getResources().getColor(R.color.black));

        BarData barData = new BarData(barDataSet1,barDataSet2, barDataSet3 , barDataSet4);
        binding.group.setData(barData);

        String[] days = new String[]{"Saturday" , "Sunday" , "Monday" , "Thursday" , "Wednesday" , "Tuesday" , "Friday"};

        XAxis xAxis = binding.group.getXAxis();
        xAxis.setValueFormatter(new IndexAxisValueFormatter(days));
        xAxis.setCenterAxisLabels(true);
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setGranularity(1);
        xAxis.setGranularityEnabled(true);


        binding.group.setDragEnabled(true);
        binding.group.setVisibleXRangeMaximum(3);


        float barSpace = 0.08f;
        float groupSpace = 0.44f;
        barData.setBarWidth(0.10f);

        binding.group.getXAxis().setAxisMaximum(0);
        binding.group.getXAxis().setAxisMaximum(0+binding.group.getBarData().getGroupWidth(groupSpace,barSpace)*7);
        binding.group.getXAxis().setAxisMinimum(0);

        binding.group.groupBars(0,groupSpace,barSpace);
        binding.group.invalidate();

    }



    private ArrayList<BarEntry> barEntries1(){
        ArrayList<BarEntry> barEntries = new ArrayList<>();
        barEntries.add(new BarEntry(1 , 2000));
        barEntries.add(new BarEntry(2 , 791));
        barEntries.add(new BarEntry(3 , 347));
        barEntries.add(new BarEntry(4 , 896));
        barEntries.add(new BarEntry(5 , 1250));
        barEntries.add(new BarEntry(6 , 364));
        barEntries.add(new BarEntry(7 , 658));


        return barEntries;
    }
    private ArrayList<BarEntry> barEntries2(){
        ArrayList<BarEntry> barEntries = new ArrayList<>();
        barEntries.add(new BarEntry(1 , 500));
        barEntries.add(new BarEntry(2 , 355));
        barEntries.add(new BarEntry(3 , 870));
        barEntries.add(new BarEntry(4 , 1900));
        barEntries.add(new BarEntry(5 , 900));
        barEntries.add(new BarEntry(6 , 2000));
        barEntries.add(new BarEntry(7 , 658));


        return barEntries;
    }

    private ArrayList<BarEntry> barEntries3(){
        ArrayList<BarEntry> barEntries = new ArrayList<>();
        barEntries.add(new BarEntry(1 , 879));
        barEntries.add(new BarEntry(2 , 45));
        barEntries.add(new BarEntry(3 , 452));
        barEntries.add(new BarEntry(4 , 178));
        barEntries.add(new BarEntry(5 , 1380));
        barEntries.add(new BarEntry(6 , 1590));
        barEntries.add(new BarEntry(7 , 658));


        return barEntries;
    }

    private ArrayList<BarEntry> barEntries4(){
        ArrayList<BarEntry> barEntries = new ArrayList<>();
        barEntries.add(new BarEntry(1 , 100));
        barEntries.add(new BarEntry(2 , 200));
        barEntries.add(new BarEntry(3 , 347));
        barEntries.add(new BarEntry(4 , 753));
        barEntries.add(new BarEntry(5 , 1111));
        barEntries.add(new BarEntry(6 , 2000));
        barEntries.add(new BarEntry(7 , 658));


        return barEntries;
    }
}