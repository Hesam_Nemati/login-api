package ir.hesam.rx_java.Chart;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.os.Bundle;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;

import java.util.ArrayList;

import ir.hesam.rx_java.R;
import ir.hesam.rx_java.databinding.ActivityChartBinding;

public class ChartActivity extends AppCompatActivity {

    ActivityChartBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this,R.layout.activity_chart);


        BarDataSet barDataSet1 = new BarDataSet(dataVlue1(),"Data set 1");
        BarDataSet barDataSet2 = new BarDataSet(datavalue2(),"data set  2");
        barDataSet1.setColor(getResources().getColor(R.color.green_font));
        barDataSet2.setColor(getResources().getColor(R.color.red));


        BarData barData = new BarData();
        barData.addDataSet(barDataSet1);
        barData.addDataSet(barDataSet2);



        binding.lineChart.setData(barData);
        binding.lineChart.invalidate();


    }

    private ArrayList<BarEntry> dataVlue1() {
        ArrayList<BarEntry> dataVals = new ArrayList<>();
        dataVals.add(new BarEntry(0,3));
        dataVals.add(new BarEntry(1,4));
        dataVals.add(new BarEntry(2,5));
        dataVals.add(new BarEntry(5,6));
        dataVals.add(new BarEntry(4,7));
        return dataVals;
    }
    private ArrayList<BarEntry> datavalue2(){
        ArrayList<BarEntry> datavals = new ArrayList<>();
        datavals.add(new BarEntry(13.4f,4));
        datavals.add(new BarEntry(13.4f,4));
        datavals.add(new BarEntry(6,8.36f));
        datavals.add(new BarEntry(7.2f,1));
        return datavals;
    }
}