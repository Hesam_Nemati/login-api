package ir.hesam.rx_java.Chart;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.graphics.Color;
import android.os.Bundle;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;

import java.util.ArrayList;

import ir.hesam.rx_java.R;
import ir.hesam.rx_java.databinding.ActivityPieBinding;

public class PieActivity extends AppCompatActivity {

    //    ActivityPieBinding binding;
//    int [] color = new int[]{Color.RED ,Color.BLUE ,Color.CYAN ,Color.DKGRAY ,Color.GREEN ,Color.YELLOW ,Color.GRAY};
//    RelativeLayout mainLayout;
//    PieChart pieChart;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        binding = DataBindingUtil.setContentView(this , R.layout.activity_pie);
//
//        PieDataSet pieDataSet = new PieDataSet(datavaluse(),"");
//        pieDataSet.setColors(color);
//
//        PieData pieData = new PieData(pieDataSet);
//
//        binding.pie.setData(pieData);
//        binding.pie.invalidate();
//        binding.pie.setDrawEntryLabels(false); // <-- if set TRUE write label of Value in chart
//        binding.pie.setUsePercentValues(true); // <-- Convert to Percentage (0-100 % )
//        binding.pie.setCenterText("Hello World"); // <-- the text of write in center of circle
//        binding.pie.setCenterTextSize(35);
//        binding.pie.setCenterTextColor(Color.RED);
//        binding.pie.setCenterTextRadiusPercent(50);
//        binding.pie.setHoleRadius(50); // <-- if more than 50 , the label will be small
//        binding.pie.setHoleColor(Color.GREEN); // <-- the color of background of Center text
//        binding.pie.setRotationEnabled(false); // <-- if that false the chart value cant Rotate
//        binding.pie.setTransparentCircleRadius(30);
//        binding.pie.setMaxAngle(360); //<-- can set angle of circle fro 360


//        mainLayout = new RelativeLayout(this);
//        RelativeLayout.LayoutParams mainLayoutParam = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
//        mainLayout.setLayoutParams(mainLayoutParam);
//
//
//        pieChart = new PieChart(this);
//        RelativeLayout.LayoutParams pieChartParam = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
//        pieChart.setLayoutParams(pieChartParam);
//        pieChartParam.addRule(RelativeLayout.CENTER_IN_PARENT);
//
//        int[] color = new int[]{Color.RED, Color.BLUE, Color.CYAN, Color.DKGRAY, Color.GREEN, Color.YELLOW, Color.GRAY};
//
//        PieDataSet pieDataSet = new PieDataSet(datavaluse(), "");
//        pieDataSet.setColors(color);
//
//        PieData pieData = new PieData(pieDataSet);
//
//
//        pieChart.setData(pieData);
//        pieChart.invalidate();
//        pieChart.setDrawEntryLabels(false); // <-- if set TRUE write label of Value in chart
//        pieChart.setUsePercentValues(true); // <-- Convert to Percentage (0-100 % )
//        pieChart.setCenterText("Hello World"); // <-- the text of write in center of circle
//        pieChart.setCenterTextSize(35);
//        pieChart.setCenterTextColor(Color.RED);
//        pieChart.setCenterTextRadiusPercent(50);
//        pieChart.setHoleRadius(50); // <-- if more than 50 , the label will be small
//        pieChart.setHoleColor(Color.GREEN); // <-- the color of background of Center text
//        pieChart.setRotationEnabled(false); // <-- if that false the chart value cant Rotate
//        pieChart.setTransparentCircleRadius(30);
//        pieChart.setMaxAngle(360); //<-- can set angle of circle fro 360
//
//
//        mainLayout.addView(pieChart, pieChartParam);
//        setContentView(mainLayout);

//        creatChart();

    }

    private ArrayList<PieEntry> datavaluse() {
        ArrayList<PieEntry> dataVals = new ArrayList<>();
        dataVals.add(new PieEntry(32, "sat"));
        dataVals.add(new PieEntry(58, "sun"));
        dataVals.add(new PieEntry(69, "mon"));
        dataVals.add(new PieEntry(75, "thr"));
        dataVals.add(new PieEntry(9, "wed"));
        dataVals.add(new PieEntry(24, "thu"));
        dataVals.add(new PieEntry(52, "fri"));

        return dataVals;
    }


    public void creatChart(){

        RelativeLayout mainLayout;
        PieChart pieChart;

        mainLayout = new RelativeLayout(this);
        RelativeLayout.LayoutParams mainLayoutParam = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        mainLayout.setLayoutParams(mainLayoutParam);


        pieChart = new PieChart(this);
        RelativeLayout.LayoutParams pieChartParam = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        pieChart.setLayoutParams(pieChartParam);
        pieChartParam.addRule(RelativeLayout.CENTER_IN_PARENT);


        int[] color = new int[]{Color.RED, Color.BLUE, Color.CYAN, Color.DKGRAY, Color.GREEN, Color.YELLOW, Color.GRAY};

        PieDataSet pieDataSet = new PieDataSet(datavaluse(), "");
        pieDataSet.setColors(color);

        PieData pieData = new PieData(pieDataSet);


        pieChart.setData(pieData);
        pieChart.invalidate();
        pieChart.setDrawEntryLabels(false); // <-- if set TRUE write label of Value in chart
        pieChart.setUsePercentValues(true); // <-- Convert to Percentage (0-100 % )
        pieChart.setCenterText("Hello World"); // <-- the text of write in center of circle
        pieChart.setCenterTextSize(35);
        pieChart.setCenterTextColor(Color.RED);
        pieChart.setCenterTextRadiusPercent(50);
        pieChart.setHoleRadius(50); // <-- if more than 50 , the label will be small
        pieChart.setHoleColor(Color.GREEN); // <-- the color of background of Center text
        pieChart.setRotationEnabled(false); // <-- if that false the chart value cant Rotate
        pieChart.setTransparentCircleRadius(30);
        pieChart.setMaxAngle(360); //<-- can set angle of circle fro 360


        mainLayout.addView(pieChart, pieChartParam);
        setContentView(mainLayout);

    }

}