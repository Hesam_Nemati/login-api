package ir.hesam.rx_java.notif;

import android.app.AlarmManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.button.MaterialButton;

import ir.hesam.rx_java.Home.HomeActivity;
import ir.hesam.rx_java.R;

public class NotificationActivity extends AppCompatActivity {

    Context context;
    RelativeLayout main;
    MaterialButton sendNotif;
    MaterialButton sendbtn;
    NotificationManager notifManager;
    String offerChannelId = "Offers";


    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        /**
         * Dynamic UI
         * {@link sendNotif} --> For send notification
         * {@link sendbtn} --> For send notification after time and set Timer in "milisec"
         */

        //region UI
        main = new RelativeLayout(this);
        sendNotif = new MaterialButton(this);
        sendbtn = new MaterialButton(this);

        RelativeLayout.LayoutParams mainParam = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        main.setLayoutParams(mainParam);


        RelativeLayout.LayoutParams sendNotifParam = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        sendNotif.setLayoutParams(sendNotifParam);
        sendNotifParam.addRule(RelativeLayout.CENTER_IN_PARENT);
        sendNotif.setText("Send Notification!");


        RelativeLayout.LayoutParams sendbtnparam = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        sendbtn.setLayoutParams(sendbtnparam);
        sendbtnparam.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
        sendbtnparam.addRule(RelativeLayout.CENTER_HORIZONTAL);
        sendbtn.setText("Send Notification Timer!");

        //endregion

        /**
         * Send notification WithOut timer
         */
        sendNotif.setOnClickListener(view -> {
//            NotificationPresenter.getInstance().showNotification(this , this);
            NotificationPresenter.getInstance().scheduleTextNotification(this, 0,
                    "مدتاک نوتیفیکیشن" ,
                    "لورم ایپسوم لورم ایپسوم لورم ایپسوم لورم ایپسوم لورم ایپسوم لورم ایپسوم لورم ایپسوم لورم ایپسوم لورم ایپسوم لورم ایپسوم لورم ایپسوم لورم ایپسوم لورم ایپسوم لورم ایپسوم لورم ایپسوم لورم ایپسوم لورم ایپسوم لورم ایپسوم لورم ایپسوم لورم ایپسوم لورم ایپسوم لورم ایپسوم لورم ایپسوم ",
                    NotificationPresenter.BILL,
                    HomeActivity.class);
        });


        /**
         * Send notification whit timer
         */
        sendbtn.setOnClickListener(view -> {
//            alarmManager.set(AlarmManager.RTC, 10000, pendingIntent);
//            NotificationPresenter.getInstance().showNotification(this, 3);
            Log.e("TAG", "onCreate:  Notif Called");
            NotificationPresenter.getInstance().scheduleNotification(this, 3000, "مدتاک نوتیفیکیشن"
                    , "لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد. کتابهای زیادی در شصت و سه درصد گذشته"
                    , NotificationPresenter.QBANK,
                    NotificationActivity.class);

        });


        /**
         * Set timer for timer notification in MiliSecond
         */
        int milisec = 30000;
//        alarmManager.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + (milisec), pendingIntent);

        main.addView(sendNotif, sendNotifParam);
        main.addView(sendbtn, sendbtnparam);
        setContentView(main);
    }
}
