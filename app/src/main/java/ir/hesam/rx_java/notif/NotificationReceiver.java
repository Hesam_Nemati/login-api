package ir.hesam.rx_java.notif;


import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.RequiresApi;

public class NotificationReceiver extends BroadcastReceiver {

    public static String NOTIFICATION_ID = "notification-id" ;
    public static String NOTIFICATION = "notification" ;


    /**
     * This activity for send notification after time that we initialize in {@link NotificationActivity}
     * @param context - the context of Activity
     * @param intent - the intent of Activity
     */

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onReceive(Context context, Intent intent) {
//        Toast.makeText(context, "notification", Toast.LENGTH_SHORT).show();
//
//        Log.e("TAG", "onReceive: " + intent.getDataString() );
//        NotificationPresenter.getInstance().showNotification(context, 5);

        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE) ;
        Notification notification = intent.getParcelableExtra(NOTIFICATION) ;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            int importance = NotificationManager. IMPORTANCE_HIGH ;
            NotificationChannel notificationChannel = new NotificationChannel("Main","NOTIFICATION_CHANNEL_NAME",importance) ;
            assert notificationManager != null;
            notificationManager.createNotificationChannel(notificationChannel) ;
        }
        int id = intent.getIntExtra(NOTIFICATION_ID,0) ;
        assert notificationManager != null;
        notificationManager.notify(id , notification) ;
    }
}
