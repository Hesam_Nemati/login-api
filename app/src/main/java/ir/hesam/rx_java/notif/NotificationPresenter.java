package ir.hesam.rx_java.notif;

import static android.content.Context.ALARM_SERVICE;

import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.os.Build;
import android.os.SystemClock;
import android.widget.RemoteViews;

import androidx.annotation.RequiresApi;

import java.util.Random;

import ir.hesam.rx_java.Home.HomeActivity;
import ir.hesam.rx_java.R;

public class NotificationPresenter {

    AlarmManager alarmManager;
    Intent set_delay;

    public static final String QBANK = "بانک سوال";
    public static final String ONLINE_CLASS = "کلاس آنلاین";
    public static final String ANNOUNCEMENT = "اطلاعیه ها";
    public static final String BILL = "صورت حساب ها";
    public static final String VOTE = "نظرسنجی ها";
    public static final String HOMEWORK = "تکلیف ها";
    public static final String GRADE_CARD = "کارنامه";

    private static NotificationPresenter ourInstance;

    public static NotificationPresenter getInstance() {
        if (ourInstance == null) {
            ourInstance = new NotificationPresenter();
        }
        return ourInstance;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public void showNotification(Context context, long sec) {

        RemoteViews collapsedView = new RemoteViews(context.getPackageName(), R.layout.nitification_collapsed);

        RemoteViews expandedView = new RemoteViews(context.getPackageName(), R.layout.nitification_expanded);

        collapsedView.setTextViewText(R.id.text_view_collapsed_1, "هم اکنون با مدتاک!");
        collapsedView.setTextViewText(R.id.text_view_collapsed_2, "فروش بسیار عالی مدتاک فروش بسیار عالی مدتاک فروش بسیار عالی مدتاک فروش بسیار عالی مدتاک فروش بسیار عالی مدتاک فروش بسیار عالی مدتاک فروش بسیار عالی مدتاک فروش بسیار عالی مدتاک ");


        expandedView.setImageViewResource(R.id.image_view_expanded, R.drawable.a);
        expandedView.setTextViewText(R.id.text_view_expanded, "1234");


        set_delay = new Intent(context, NotificationReceiver.class);

        alarmManager = (AlarmManager) context.getSystemService(ALARM_SERVICE);

        PendingIntent delayIntent = PendingIntent.getBroadcast(
                context, 234, set_delay, PendingIntent.FLAG_IMMUTABLE);
        alarmManager.set(AlarmManager.RTC, sec * 1000, delayIntent);

        Intent intent = new Intent(context, HomeActivity.class);
        String CHANNEL_ID = "MYCHANNEL";
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 1, intent, PendingIntent.FLAG_IMMUTABLE);

        expandedView.setOnClickPendingIntent(R.id.image_view_expanded, pendingIntent);
        expandedView.setOnClickPendingIntent(R.id.expanded_ll, pendingIntent);
//        collapsedView.setOnClickPendingIntent(R.id.collapsed_ll, pendingIntent);
        alarmManager.set(AlarmManager.RTC_WAKEUP, sec * 1000, delayIntent);

        NotificationChannel notificationChannel = new NotificationChannel(CHANNEL_ID, "name", NotificationManager.IMPORTANCE_LOW);

        @SuppressLint({"WrongConstant", "ResourceAsColor"}) Notification notification = new Notification.Builder(context, CHANNEL_ID)
//                .setContentTitle("پیشنهاد ویژه!!!")
//                .setContentText("خرید پنل احتصاصی کلاس آنلاین فروش بسیار عالی مدتاک فروش بسیار عالی مدتاک فروش بسیار عالی مدتاک فروش بسیار عالی مدتاک فروش بسیار عالی مدتاک فروش بسیار عالی مدتاک فروش بسیار عالی مدتاک فروش بسیار عالی مدتاک ")
                .setContentIntent(pendingIntent)
                .setCustomContentView(collapsedView)
                .setCustomBigContentView(expandedView)
                .setChannelId(CHANNEL_ID)
                .setAutoCancel(true)
//                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                .setSmallIcon(R.drawable.cheque_icon)
                .setStyle(new Notification.DecoratedCustomViewStyle())
                .setWhen(System.currentTimeMillis())
                .addAction(0, "مشاهده", pendingIntent)
//                .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.call))
//                .setPriority(NotificationCompat.PRIORITY_HIGH)
//                .setCategory(NotificationCompat.CATEGORY_CALL)
//                .setColor(R.color.red)
//                .setColorized(true)
//                .setVibrate(new long[]{100,500,500,500,500,500})
                .build();


        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.createNotificationChannel(notificationChannel);
        notificationManager.notify(1, notification);
//        notificationChannel.setLightColor(Color.WHITE);
//        notificationChannel.enableLights(true);
//        notificationChannel.enableLights(true);
//        notificationChannel.enableVibration(true);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public void showNotification(Context context, NotificationActivity activity) {

        RemoteViews collapsedView = new RemoteViews(context.getPackageName(), R.layout.nitification_collapsed);

        RemoteViews expandedView = new RemoteViews(context.getPackageName(), R.layout.nitification_expanded);

        collapsedView.setTextViewText(R.id.text_view_collapsed_1, "برگزاری آزمون ریاضی فقط با یک کلیک");
        collapsedView.setTextViewText(R.id.text_view_collapsed_2, " ");


        expandedView.setImageViewResource(R.id.image_view_expanded, R.drawable.pic);
        expandedView.setTextViewText(R.id.text_view_expanded, "آزمون ریاضی (با میزان سختی متوسط و زمان 30 دقیقه) آماده برگزاری است. با یک کلیک، به سادگی اطلاعات دانش آموزان خود را بسنجید");


        Intent i = new Intent(context, NotificationActivity.class);
        PendingIntent click = PendingIntent.getBroadcast(context, 0, i, 0);

        expandedView.setOnClickPendingIntent(R.id.image_view_expanded, click);

        Intent intent = new Intent(context, HomeActivity.class);
        String CHANNEL_ID = "MYCHANNEL";

        NotificationChannel notificationChannel = new NotificationChannel(CHANNEL_ID, "name", NotificationManager.IMPORTANCE_LOW);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 1, intent, 0);
        @SuppressLint({"WrongConstant", "ResourceAsColor"}) Notification notification = new Notification.Builder(context, CHANNEL_ID)
                .setContentIntent(pendingIntent)
                .setCustomContentView(collapsedView)
                .setCustomBigContentView(expandedView)
                .setChannelId(CHANNEL_ID)
                .setAutoCancel(true)
//                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                .setSmallIcon(R.drawable.logo)
                .setStyle(new Notification.DecoratedCustomViewStyle())
                .setWhen(System.currentTimeMillis())
                .addAction(0, "مشاهده", pendingIntent)
//                .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.call))
//                .setPriority(NotificationCompat.PRIORITY_HIGH)
//                .setCategory(NotificationCompat.CATEGORY_CALL)
//                .setColor(R.color.red)
//                .setColorized(true)
//                .setVibrate(new long[]{100,500,500,500,500,500})
                .setShowWhen(true)
                .build();


        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
//        notificationManager.createNotificationChannel(notificationChannel);
        notificationManager.notify(1, notification);
//        notificationChannel.setLightColor(Color.WHITE);
//        notificationChannel.enableLights(true);
//        notificationChannel.enableLights(true);
//        notificationChannel.enableVibration(true);
    }


    //    @RequiresApi(api = Build.VERSION_CODES.O)
    public void scheduleNotification(Context context, long delay, String title, String content, String channel_id, Class destination) {

        //region initialize views
        RemoteViews collapsedView = new RemoteViews(context.getPackageName(), R.layout.nitification_collapsed);
        RemoteViews expandedView = new RemoteViews(context.getPackageName(), R.layout.nitification_expanded);

        collapsedView.setTextViewText(R.id.text_view_collapsed_1, title);
        collapsedView.setTextViewText(R.id.text_view_collapsed_2, content);

        expandedView.setImageViewResource(R.id.image_view_expanded, R.drawable.a);
        expandedView.setTextViewText(R.id.text_view_expanded, content);
        expandedView.setTextViewText(R.id.text_view_expanded_title, title);
        //endregion

        //region random id of single notification
        Random random = new Random();
        int random_id = random.nextInt(9999 - 1000) + 1000000;
        //endregion

        //region Choose destination Activity
        Intent intent = new Intent(context, destination);

        PendingIntent pending = PendingIntent.getActivity(context, random_id, intent, PendingIntent.FLAG_IMMUTABLE);
        Intent notificationIntent = new Intent(context, NotificationReceiver.class);

        expandedView.setOnClickPendingIntent(R.id.image_view_expanded, pending);
        expandedView.setOnClickPendingIntent(R.id.expanded_ll, pending);
//        collapsedView.setOnClickPendingIntent(R.id.collapsed_ll, pending);
        //endregion

        //region Build Notification
        NotificationChannel notificationChannel = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            notificationChannel = new NotificationChannel(channel_id, channel_id, NotificationManager.IMPORTANCE_HIGH);

            //---------------------------------------
            Notification builder = new Notification.Builder(context, channel_id)
                    .setContentTitle(title)
                    .setContentText(content)
                    .setChannelId(channel_id)
                    .setContentIntent(pending)
                    .setCustomContentView(collapsedView)
                    .setCustomBigContentView(expandedView)
                    .setAutoCancel(true)
                    .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                    .setSmallIcon(R.drawable.cheque_icon)
//                    .addAction(0, "مشاهده", pending)
                    .setActions(new Notification.Action[]{
                            new Notification.Action(R.drawable.cheque_icon, "مشاهده", pending)})
                    .setStyle(new Notification.DecoratedCustomViewStyle())
                    .setWhen(System.currentTimeMillis())
                    .setColorized(true)
                    .setShowWhen(true)
//                .setLargeIcon(BitmapFactory.decodeResource(activity.getResources(), R.drawable.call))
//                .setCategory(NotificationCompat.CATEGORY_CALL)
//                .setColor(R.color.red)
                    .setColorized(true)
                    .setVibrate(new long[]{100, 500, 500, 500, 500, 500})
                    .build();

            NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.createNotificationChannel(notificationChannel);
//          notificationManager.notify(1, builder);

            notificationIntent.putExtra(NotificationReceiver.NOTIFICATION, builder);
        }
        //endregion

        //region Set Configs of Notification
        notificationIntent.putExtra(NotificationReceiver.NOTIFICATION_ID, random_id);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, random_id, notificationIntent, PendingIntent.FLAG_IMMUTABLE);
        long futureInMillis = SystemClock.elapsedRealtime() + delay;
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        assert alarmManager != null;
        alarmManager.set(AlarmManager.ELAPSED_REALTIME_WAKEUP, futureInMillis, pendingIntent);
        //endregion


    }


    public void scheduleTextNotification(Context context, long delay, String title, String content, String channel_id, Class destination) {

        /* region initialize views */
        RemoteViews collapsedView = new RemoteViews(context.getPackageName(), R.layout.nitification_collapsed);
//        RemoteViews expandedView = new RemoteViews(context.getPackageName(), R.layout.notification_text_base);

        collapsedView.setTextViewText(R.id.text_view_collapsed_1, title);
        collapsedView.setTextViewText(R.id.text_view_collapsed_2, content);

//        expandedView.setImageViewResource(R.id.image_view_expanded, R.drawable.a);

//        expandedView.setTextViewText(R.id.text_notif_content_tv, content);
//        expandedView.setTextViewText(R.id.text_notif_title_tv, title);
        //endregion

        //region random id of single notification
        Random random = new Random();
        int random_id = random.nextInt(9999 - 1000) + 1000000;
        //endregion

        //region Choose destination Activity
        Intent intent = new Intent(context, destination);

        PendingIntent pending = PendingIntent.getActivity(context, random_id, intent, PendingIntent.FLAG_IMMUTABLE);
        Intent notificationIntent = new Intent(context, NotificationReceiver.class);

//        expandedView.setOnClickPendingIntent(R.id.image_view_expanded, pending);
//        expandedView.setOnClickPendingIntent(R.id.expanded_ll, pending);
//        collapsedView.setOnClickPendingIntent(R.id.collapsed_ll, pending);
        //endregion

        //region Build Notification
        NotificationChannel notificationChannel = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            notificationChannel = new NotificationChannel(channel_id, channel_id, NotificationManager.IMPORTANCE_HIGH);

            //---------------------------------------
            @SuppressLint({"WrongConstant", "ResourceAsColor"}) Notification builder = new Notification.Builder(context, channel_id)
                    .setContentTitle(title)
                    .setContentText(content)
                    .setAutoCancel(true)
                    .setChannelId(channel_id)
                    .setContentIntent(pending)
                    .setCustomContentView(collapsedView)
//                    .setCustomBigContentView(expandedView)
                    .setAutoCancel(true)
                    .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                    .setSmallIcon(R.drawable.cheque_icon)
//                    .addAction(0, "مشاهده", pending)
                    .setActions(new Notification.Action[]{new Notification.Action(R.drawable.cheque_icon, "مشاهده", pending)})
                    .setStyle(new Notification.DecoratedCustomViewStyle())
                    .setWhen(System.currentTimeMillis())
                    .setColorized(true)
                    .setShowWhen(true)
                    .setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.drawable.pic))
//                .setCategory(NotificationCompat.CATEGORY_CALL)
//                .setColor(R.color.red)
                    .setColorized(true)
                    .setVibrate(new long[]{100, 500, 500, 500, 500, 500})
                    .build();

            NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.createNotificationChannel(notificationChannel);
//          notificationManager.notify(1, builder);

            notificationIntent.putExtra(NotificationReceiver.NOTIFICATION, builder);
        }
        //endregion

        //region Set Configs of Notification
        notificationIntent.putExtra(NotificationReceiver.NOTIFICATION_ID, random_id);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, random_id, notificationIntent, PendingIntent.FLAG_IMMUTABLE);
        long futureInMillis = SystemClock.elapsedRealtime() + delay;
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        assert alarmManager != null;
        alarmManager.set(AlarmManager.ELAPSED_REALTIME_WAKEUP, futureInMillis, pendingIntent);
        //endregion


    }

}
