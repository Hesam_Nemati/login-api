package ir.hesam.rx_java.module;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class RecordedClassResult {

    @SerializedName("result")
    private ArrayList<RecordedFileList> recordedFileLists;


    public ArrayList<RecordedFileList> getRecordedFileLists() {
        return recordedFileLists;
    }
}
