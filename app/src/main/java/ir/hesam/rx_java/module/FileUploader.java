package ir.hesam.rx_java.module;

import android.net.Uri;

import java.io.Serializable;
public class FileUploader implements Serializable {
    private Uri uri;
    private String path;
    private String extension;
    private int progress;
    private String id;

    public FileUploader(String path,String id) {
        this.path = path;

        this.extension = FileUtils.file_ext(path);

        this.id =id;

//        Timber.e(id);
    }

}
