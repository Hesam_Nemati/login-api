package ir.hesam.rx_java.module.ReponseBody;

import com.google.gson.annotations.SerializedName;

public class user {

    @SerializedName("first_name")
    String first_name;

    @SerializedName("last_name")
    String last_name;

    @SerializedName("username")
    String username;

    @SerializedName("type")
    String type;


    public String getFirst_name() {
        return first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public String getUsername() {
        return username;
    }

    public String getType() {
        return type;
    }
    @Override
    public String toString() {
        return "user{" +
                "first_name='" + first_name + '\'' +
                ", last_name='" + last_name + '\'' +
                ", username='" + username + '\'' +
                ", type='" + type + '\'' +
                '}';
    }

}
