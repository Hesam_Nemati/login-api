package ir.hesam.rx_java.module;

import com.google.gson.annotations.SerializedName;

public class FileResponse {


    @SerializedName("file")
    private String file;
    @SerializedName("user_id")
    private int user_id;
    @SerializedName("title")
    private String title;
    @SerializedName("id")
    private int id;


    public String getFile() {
        return file;
    }

    public int getUser_id() {
        return user_id;
    }

    public String getTitle() {
        return title;
    }

    public int getId() {
        return id;
    }
}
