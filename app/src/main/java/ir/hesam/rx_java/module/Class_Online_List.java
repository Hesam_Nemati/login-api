package ir.hesam.rx_java.module;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Class_Online_List {

    @SerializedName("result")
    private ArrayList<ClassOnlineList> classOnline;


    public ArrayList<ClassOnlineList> getClassOnline() {
        return classOnline;
    }
}
