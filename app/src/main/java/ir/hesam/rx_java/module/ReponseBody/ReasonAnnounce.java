package ir.hesam.rx_java.module.ReponseBody;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;


public class ReasonAnnounce implements Serializable {

    @SerializedName("subject")
    private String title;

    @SerializedName("date_time")
    private String date;

    @SerializedName("text")
    private String text;

    public int getId() {
        return id;
    }

    @SerializedName("id")
    private int id;

    @SerializedName("seen")
    private boolean seen;

    @SerializedName("banner")
    private String banner;


    @SerializedName("is_announcement")
    private boolean is_announcement;




    public boolean is_announcement() {
        return is_announcement;
    }




    public String getBanner() {
        return banner;
    }

    public boolean isSeen() {
        return seen;
    }

    public String getTitle() {
        return title;
    }


    public String getDate() {
        return date;
    }

    public String getText() {
        return text;
    }

    public ReasonAnnounce() {
    }


}
