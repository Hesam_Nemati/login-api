package ir.hesam.rx_java.module;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class RecordedFileList implements Serializable {

    @SerializedName("date_time")
    String date_time;

    @SerializedName("link")
    String link;

    @SerializedName("name")
    String name;


    public String getDate_time() {
        return date_time;
    }

    public String getLink() {
        return link;
    }

    public String getName() {
        return name;
    }


    @Override
    public String toString() {
      return "RecordedFileList{" +
              "date_time='" + date_time + '\'' +
              ", link='" + link + '\'' +
              ", name='" + name + '\'' +
              '}';
  }

}

