package ir.hesam.rx_java.module;

import com.google.gson.annotations.SerializedName;

public class JoinClass {
    @SerializedName("result")
    private String url;
    @SerializedName("is_info")
    private boolean is_info;
    @SerializedName("is_skyroom")
    private boolean is_skyroom;


    public boolean isIs_info() {
        return url.contains("getMeetingInfo");
//        return is_info;
    }

    public String getUrl() {
        return url;
    }

    public boolean isIs_skyroom() {
        return is_skyroom;
    }
}
