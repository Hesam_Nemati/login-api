package ir.hesam.rx_java.module.ReponseBody;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class AnnounceList {


    @SerializedName("count")
    int count ;

    @SerializedName("result")
    private ArrayList<ReasonAnnounce> announces;


    public ArrayList<ReasonAnnounce> getAnnounces() {
        return announces;
    }
}
