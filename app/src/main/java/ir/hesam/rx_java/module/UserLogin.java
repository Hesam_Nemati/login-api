package ir.hesam.rx_java.module;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;import ir.hesam.rx_java.UserAuth;

public class UserLogin implements Serializable {
    @SerializedName("id")
    private String id;

    @SerializedName("user")
    private UserAuth user;

    @SerializedName("mobile_phone")
    private String mobile_phone;

    @SerializedName("national_id")
    private String national_id;

    @SerializedName("access")
    private String access;


    @SerializedName("refresh")
    private String refresh;

    @SerializedName("school_str")
    private String school_str;


    public String getSchool_str() {
        return school_str;
    }

    public String getId() {
        return id;
    }

    public UserAuth getUser() {
        return user;
    }

    public String getMobile_phone() {
        return mobile_phone;
    }

    public String getNational_id() {
        return national_id;
    }

    public String getAccess() {
        return access;
    }

    public String getRefresh() {
        return refresh;
    }
    /**
     * #include <iostream>
     * #include <math.h>
     *
     * using namespace std;
     *
     * int main()
     * {
     *     int *n, i;
     *     n[0] = 1;
     *     for(i=0;n[i];i++){
     *         cin >> n[i];
     *     }
     *     int max=0;
     *     for(int j=0;j<i-1;j++){
     *         if (abs(n[j]-n[j+1])>max){
     *             max = abs(n[j]-n[j+1]);
     *         }
     *     }
     *     cout << max;
     * }
     */
}
