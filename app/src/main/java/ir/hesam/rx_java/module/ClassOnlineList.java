package ir.hesam.rx_java.module;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ClassOnlineList implements Serializable {

    @SerializedName("id")
    int id;

    @SerializedName("name")
    String name;

    @SerializedName("school")
    String school;

    @SerializedName("section_str")
    String section_str;


    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getSchool() {
        return school;
    }

    public String getSection_str() {
        return section_str;
    }

    @Override
    public String toString() {
        return "ClassOnlineList{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", school='" + school + '\'' +
                ", section_str='" + section_str + '\'' +
                '}';
    }

    public ClassOnlineList() {
    }
}
