package ir.hesam.rx_java.ClassOnline;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.recyclerview.widget.RecyclerView;

import com.sothree.slidinguppanel.SlidingUpPanelLayout;

import java.util.ArrayList;

import ir.hesam.rx_java.module.ClassOnlineList;
import ir.hesam.rx_java.R;
import ir.hesam.rx_java.databinding.ClassOnlineBaseBinding;

public class ClassOnlineAdapter extends RecyclerView.Adapter<ClassOnlineAdapter.VersionViewHolder> {

    ClassOnlineActivity classOnlineActivity;
    ArrayList<ClassOnlineList> lists = new ArrayList<>();

    public ClassOnlineAdapter( ClassOnlineActivity context, ArrayList<ClassOnlineList> lists) {
        this.classOnlineActivity = context;
        this.lists = lists;
    }


    @Override
    public ClassOnlineAdapter.VersionViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.class_online_base, parent, false);
        ClassOnlineAdapter.VersionViewHolder viewHolder = new ClassOnlineAdapter.VersionViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final VersionViewHolder holder, final int position) {
        //position -> current item
        holder.binding.sectionStr.setText(lists.get(position).getSection_str());
        holder.binding.name.setText(lists.get(position).getName());
    }

    @Override
    public int getItemCount() {
        return lists == null ? 0 : lists.size();
    }

    public class VersionViewHolder extends RecyclerView.ViewHolder {

        ClassOnlineBaseBinding binding;


        public VersionViewHolder(View itemView) {
            super(itemView);
            binding = ClassOnlineBaseBinding.bind(itemView);

        //upload File
            binding.uploadBtn.setOnClickListener(view -> {

                // Initializing Second Layout Of SlidingPanel
                classOnlineActivity.binding.slidablePanel.setPanelState(SlidingUpPanelLayout.PanelState.EXPANDED);
                classOnlineActivity.binding.name.setText(lists.get(getAdapterPosition()).getName());
                classOnlineActivity.binding.sectionStr.setText(lists.get(getAdapterPosition()).getSection_str());

                classOnlineActivity.binding.toolbarList.iconRToolbar.setOnClickListener(view1 -> {
                    ClassOnlinePresenter.getInstance().pickFile(classOnlineActivity);
                });

            });

        // enter Class
            binding.enterClass.setOnClickListener(view -> {
                ClassOnlinePresenter.getInstance().goto_online_class(classOnlineActivity,lists.get(getAdapterPosition()));
               // lists.get(getAdapterPosition()); <-- Get Position Of Item In RecyclerView
            });

        // Recorded Files
            binding.recorded.setOnClickListener(view -> {
                ClassOnlinePresenter.getInstance().goto_recorded(classOnlineActivity);
            });

        }
    }
}
