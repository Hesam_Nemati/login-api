package ir.hesam.rx_java.ClassOnline;

import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import ir.hesam.rx_java.G;
import ir.hesam.rx_java.module.ClassOnlineList;
import ir.hesam.rx_java.module.Class_Online_List;
import ir.hesam.rx_java.module.FileResponse;
import ir.hesam.rx_java.module.FileUtils;
import ir.hesam.rx_java.module.JoinClass;
import ir.hesam.rx_java.module.ProgressRequestBody;
import ir.hesam.rx_java.module.RecordedClassResult;
import ir.hesam.rx_java.NetWork.API.NetworkLayer;
import okhttp3.MultipartBody;

public class ClassOnlinePresenter {

    private static final ClassOnlinePresenter ourInstance = new ClassOnlinePresenter();


    public static ClassOnlinePresenter getInstance() {
        return ourInstance;
    }

    public ClassOnlinePresenter() {
    }


    public void setLayoutManager(ClassOnlineActivity activity, ArrayList<ClassOnlineList> lists) {

        /***
         *   LinearLayoutManager For Show An Item Just Vertical Or Horizontal
          LinearLayoutManager layoutManager
             = new LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false);
             activity.binding.recyclerClassOnline.setLayoutManager(layoutManager);
         */

        // GridLayoutManager Help To Make More Than One Item In RecyclerView
        // SpanCount : Is Integer Of We Need Show In RecyclerView
        GridLayoutManager manager = new GridLayoutManager(activity, 2, RecyclerView.VERTICAL, false);
        activity.binding.recyclerClassOnline.setLayoutManager(manager);

        ClassOnlineAdapter classOnlineAdapter;
        classOnlineAdapter = new ClassOnlineAdapter(activity, lists);
        activity.binding.recyclerClassOnline.setAdapter(classOnlineAdapter);
        classOnlineAdapter.notifyDataSetChanged();


    }


    public void callApiAdapter(ClassOnlineActivity activity) {
        NetworkLayer.getInstance().get_classOnline(G.getToken())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<Class_Online_List>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {

                    }

                    @Override
                    public void onSuccess(@NonNull Class_Online_List class_online_list) {
                        setLayoutManager(activity, class_online_list.getClassOnline());
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {


                    }
                });
    }

    // Open Activity in Phone For Choose File For Upload
    public void pickFile(ClassOnlineActivity activity) {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_GET_CONTENT);
        intent.setType("*/*"); // <-- All Type File Can Choose
        intent = Intent.createChooser(intent, "انتخاب فایل");
        activity.startActivityForResult(intent, activity.FILE_REQ);
    }


    // The Most Important Function for Upload
    // Call Api
    // Call RequestBody
    public void uploadFile(ClassOnlineActivity activity) {
        MultipartBody.Part body = null;

        if (activity.file != null) {
            activity.fileBody = new ProgressRequestBody(activity.file, activity.file.getName(), (ProgressRequestBody.UploadCallbacks) activity);
            body = MultipartBody.Part.createFormData("file", FileUtils.file_name(activity.file), activity.fileBody);
        }

        NetworkLayer.getInstance().new_file(G.getToken(), body)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<FileResponse>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {

                    }

                    @Override
                    public void onSuccess(@NonNull FileResponse fileResponse) {

                        Toast.makeText(activity, "Uploaded", Toast.LENGTH_SHORT).show();
                        Log.e("succes", "uploaded");
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        Log.e("error", "not uploaded");

                    }
                });

    }

    public void goto_online_class(ClassOnlineActivity activity, ClassOnlineList classOnlineList) {
        NetworkLayer.getInstance().join_online_class(G.getToken(), classOnlineList.getId())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<JoinClass>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {
                    }

                    @Override
                    public void onSuccess(@NonNull JoinClass joinClass) {
                        Toast.makeText(activity, "join", Toast.LENGTH_SHORT).show();
                        activity.binding.link.setText(joinClass.getUrl());
                        Intent i = new Intent(Intent.ACTION_VIEW);
                        i.setData(Uri.parse(joinClass.getUrl()));
                        activity.startActivity(i);
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        Toast.makeText(activity, "not join", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    public void goto_recorded(ClassOnlineActivity activity){
        NetworkLayer.getInstance().go_to_recoredeFile(G.getToken() , activity.classOnlineList.getId())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new SingleObserver<RecordedClassResult>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {
                    }

                    @Override
                    public void onSuccess(@NonNull RecordedClassResult recordedClassResult) {
                        Toast.makeText(activity, "join", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        Toast.makeText(activity, "not join", Toast.LENGTH_SHORT).show();
                    }
                });
    }
}