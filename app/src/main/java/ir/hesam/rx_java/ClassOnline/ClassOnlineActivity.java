package ir.hesam.rx_java.ClassOnline;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;


import com.sothree.slidinguppanel.SlidingUpPanelLayout;

import java.io.File;
import java.util.ArrayList;

import io.reactivex.annotations.Nullable;
import ir.hesam.rx_java.module.ClassOnlineList;
import ir.hesam.rx_java.module.ProgressRequestBody;
import ir.hesam.rx_java.R;
import ir.hesam.rx_java.databinding.ActivityClassOnlineBinding;


public class ClassOnlineActivity extends AppCompatActivity {
//        implements PickiTCallbacks , ProgressRequestBody.UploadCallbacks{

    public static final int FILE_REQ = 859;
    ActivityClassOnlineBinding binding;
//    PickiT pickiT;
    File file;
    ProgressRequestBody fileBody;
    ClassOnlineList classOnlineList;
    ClassOnlineAdapter classOnlineAdapter;

    //ToolbarBinding toolbarBinding; <-- Is NOT CORRECT , We Need Binding In Layout We Dont Need Binding In Toolbar
    // Just Set Id For Every Toolbar

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this , R.layout.activity_class_online);
       // toolbarBinding = DataBindingUtil.setContentView(this , R.layout.toolbar); <-- Is NOT CORRECT

        //Set Toolbar for First Layout
        binding.toolbarRecycler.title.setText("لیست کلاس های آنلاین");
        binding.toolbarRecycler.smallTittle.setText("دوازدهم");

        // Set Toolbar for Second Layout
        binding.toolbarList.title.setText("ویرایش کلاس آنلاین");
        binding.toolbarList.iconLToolbar.setImageResource(R.drawable.ic_baseline_send_24);
        binding.toolbarList.iconRToolbar.setImageResource(R.drawable.ic_baseline_attachment_24);
        binding.toolbarList.smallTittle.setVisibility(View.GONE);


        ClassOnlinePresenter.getInstance().callApiAdapter(this);


        /*
         * Pick-T for Activity -->  { pickiT = new PickiT(this, this, this); }
         * Pick-T for Fragment -->  { pickiT = new PickiT(getContext(), this, getActivity());
         */
//        pickiT = new PickiT(this , this , this);


// ----- Here Is The Best Where For Set OnClickListener
            binding.toolbarRecycler.toolbar.setNavigationOnClickListener((View.OnClickListener) v -> onBackPressed());
            binding.toolbarList.backIcon.setOnClickListener(view -> {
                binding.slidablePanel.setPanelState(SlidingUpPanelLayout.PanelState.HIDDEN);
            });
            binding.toolbarRecycler.backIcon.setOnClickListener(view -> { onBackPressed(); });


    }
   


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            if (requestCode == FILE_REQ )

                if (data != null) {
                    Uri uri = data.getData();
                    Log.e("uri: " , uri.getPath());
                }
//            pickiT.getPath(data.getData(), Build.VERSION.SDK_INT);

        }
    }


//    @Override
//    public void PickiTonUriReturned() {
//
//    }
//
//    @Override
//    public void PickiTonStartListener() {
//
//    }
//
//    @Override
//    public void PickiTonProgressUpdate(int progress) {
//
//    }
//
//    @Override
//    public void PickiTonCompleteListener(String path, boolean wasDriveFile, boolean wasUnknownProvider, boolean wasSuccessful, String Reason) {
//
//        Log.e("path: " , path);
//
//
//        this.file = new File(path);
//        ClassOnlinePresenter.getInstance().uploadFile(this);
//
//
//    }
//
//    @Override
//    public void PickiTonMultipleCompleteListener(ArrayList<String> paths, boolean wasSuccessful, String Reason) {
//
//    }
//
//    @Override
//    public void onPointerCaptureChanged(boolean hasCapture) {
//
//    }
//
//    @Override
//    public void onProgressUpdate(int percentage) {
//
//    }
//
//    @Override
//    public void onError() {
//
//    }
//
//    @Override
//    public void onFinish() {
//
//    }
}