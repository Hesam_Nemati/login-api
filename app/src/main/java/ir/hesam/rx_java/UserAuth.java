package ir.hesam.rx_java;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class UserAuth implements Serializable {
    @SerializedName("first_name")
    private String first_name;
    @SerializedName("last_name")
    private String last_name;
    @SerializedName("school")
    private int school;
    @SerializedName("username")
    private String username;
    @SerializedName("token")
    private String token;
    @SerializedName("type")
    private String type;


    public String getName() {
        return first_name+" "+last_name;
    }

    public int getSchool() {
        return school;
    }

    public String getUsername() {
        return username;
    }

    public String getToken() {
        return token;
    }

    public String getType() {
        return type;
    }


    public String get_persian_type(){
        switch (type){
            case "Manager":
                return "مدیر";
        }
        return "";
    }

    @Override
    public String toString() {
        return "UserAuth{" +
                "name='" + getName() + '\'' +
                ", school='" + school + '\'' +
                ", username='" + username + '\'' +
                ", token='" + token + '\'' +
                ", type='" + type + '\'' +
                '}';
    }
}
