package ir.hesam.rx_java.Home.Announce;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;

import java.util.ArrayList;

import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import ir.hesam.rx_java.G;
import ir.hesam.rx_java.module.Elanat;
import ir.hesam.rx_java.module.ReponseBody.AnnounceList;
import ir.hesam.rx_java.module.ReponseBody.ReasonAnnounce;
import ir.hesam.rx_java.NetWork.API.NetworkLayer;
import ir.hesam.rx_java.R;
import ir.hesam.rx_java.databinding.FragmentHomeBinding;

public class HomeFragment extends Fragment {

    private static final HomeFragment ourInstance = new HomeFragment();
    public static HomeFragment getInstance(){return ourInstance;}

    ArrayList<Elanat> elanatss;
    ArrayList<ReasonAnnounce> lists ;

    FragmentHomeBinding binding;
    Context context;
    AnnounceAdapter announceAdapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

         binding = DataBindingUtil.inflate(inflater, R.layout.fragment_home, container, false);
        View view = binding.getRoot(); // <-- Binding In Fragment

        getAnnounce();

        return view; // <-- For Fragment

    }

    public void getAnnounce(){
        NetworkLayer.getInstance().get_announce(G.getToken())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<AnnounceList>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {

                    }

                    @Override
                    public void onSuccess(@NonNull AnnounceList announceList) {

                        /*
                        SetLayoutManager And Adapter In Api Request
                         */
                        LinearLayoutManager layoutManager
                                = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
                        binding.recycler.setLayoutManager(layoutManager);
                        announceAdapter = new AnnounceAdapter(getContext(), announceList.getAnnounces());
                        announceAdapter.notifyDataSetChanged();
                        binding.recycler.setAdapter(announceAdapter);

                    }

                    @Override
                    public void onError(@NonNull Throwable e) {

                    }
                });
        }
}

