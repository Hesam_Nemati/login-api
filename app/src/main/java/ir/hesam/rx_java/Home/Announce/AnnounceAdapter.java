package ir.hesam.rx_java.Home.Announce;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import ir.hesam.rx_java.module.ReponseBody.ReasonAnnounce;
import ir.hesam.rx_java.R;
import ir.hesam.rx_java.databinding.ItemBaseBinding;

public class AnnounceAdapter extends  RecyclerView.Adapter<AnnounceAdapter.VersionViewHolder> {
    Context context;

    ArrayList<ReasonAnnounce> lists = new ArrayList<>();

    public AnnounceAdapter(Context context, ArrayList<ReasonAnnounce> lists) {
        this.context = context;

        this.lists = lists;

       // Log.e("adapter", lists.toString());
    }


    @Override
    public VersionViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_base, parent, false);
        AnnounceAdapter.VersionViewHolder viewHolder_Elanat = new AnnounceAdapter.VersionViewHolder(view);

        return viewHolder_Elanat;
    }

    @Override
    public void onBindViewHolder(final VersionViewHolder holder, final int position) {

        //position -> current item
        holder.binding.elanatText.setText(lists.get(position).getText());
        holder.binding.title.setText(lists.get(position).getTitle());
        holder.binding.time.setText(lists.get(position).getDate());


    }

    @Override
    public int getItemCount() {
        return lists == null ? 0 : lists.size();
    }

    public class VersionViewHolder extends RecyclerView.ViewHolder {

        ItemBaseBinding binding;
        TextView tit;

        public VersionViewHolder(View itemView) {
            super(itemView);
            binding = ItemBaseBinding.bind(itemView);
        }
    }
}



