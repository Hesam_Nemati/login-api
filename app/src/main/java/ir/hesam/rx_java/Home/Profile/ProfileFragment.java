package ir.hesam.rx_java.Home.Profile;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import ir.hesam.rx_java.ClassOnline.ClassOnlineActivity;
import ir.hesam.rx_java.G;
import ir.hesam.rx_java.R;
import ir.hesam.rx_java.TestActivity;
import ir.hesam.rx_java.databinding.FragmentProfileBinding;
import ir.madtalk.android.rollcall.rollCall.RollCallActivity;

public class ProfileFragment extends Fragment {

    private static final ProfileFragment ourInstance = new ProfileFragment();
    public static ProfileFragment getInstance(){return ourInstance;}


    FragmentProfileBinding binding;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_profile, container, false);
        View view = binding.getRoot();

        binding.firstName.setText(G.getName());
        Log.e("TAG", "onCreateView: "+  G.getName() );
        binding.username.setText(G.getUsername());
        binding.type.setText(G.getType());

        binding.classOnlineBtn.setOnClickListener(view1 -> {
            Intent intent = new Intent(getContext(), ClassOnlineActivity.class);
            startActivity(intent);
        });

        binding.test.setOnClickListener(view1 -> {
            Intent intent = new Intent(getContext(), TestActivity.class);
            startActivity(intent);
        });

        binding.SmartTabLayout.setOnClickListener(view1 -> {
            Intent intent = new Intent(getContext(), RollCallActivity.class);
            startActivity(intent);
        });

        return view;
    }
}
