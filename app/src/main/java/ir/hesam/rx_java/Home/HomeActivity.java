package ir.hesam.rx_java.Home;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import ir.hesam.rx_java.Home.Announce.HomeFragment;
import ir.hesam.rx_java.Home.Profile.ProfileFragment;
import ir.hesam.rx_java.Home.Upload.UploadFragment;
import ir.hesam.rx_java.R;
import ir.hesam.rx_java.databinding.ActivityHomeBinding;

public class HomeActivity extends AppCompatActivity {

    BottomNavigationView  bottomNavigationView;
    ActivityHomeBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this , R.layout.activity_home);



//        FragmentManager manager = getSupportFragmentManager();
//        FragmentTransaction fragmentTransaction = manager.beginTransaction();q
//        fragmentTransaction.add(R.id.container , new UploadFragment());
//        fragmentTransaction.commit();

        binding.bottomNavigation.setOnNavigationItemSelectedListener(item -> {




            if (item.getItemId() == R.id.upload){
               Toast.makeText(HomeActivity.this, "Upload", Toast.LENGTH_SHORT).show();
                //getFragment(new UploadFragment());
                getFragment(UploadFragment.getInstance());


            }
            else if (item.getItemId() == R.id.home){
                Toast.makeText(HomeActivity.this, "home", Toast.LENGTH_SHORT).show();
                //getFragment(new HomeFragment());
                getFragment(HomeFragment.getInstance());


            }
            else if (item.getItemId() == R.id.account){
                Toast.makeText(HomeActivity.this, "account", Toast.LENGTH_SHORT).show();
                //getFragment(new ProfileFragment());
                getFragment(ProfileFragment.getInstance());


            }
            return true;

        });
    }

    private void getFragment(Fragment fragment) {
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = manager.beginTransaction();
        fragmentTransaction.replace(R.id.frame_layout , fragment );
        fragmentTransaction.commit();
    }

}



