package ir.hesam.rx_java.Home.Upload;

import static android.app.Activity.RESULT_OK;

import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

//import com.hbisoft.pickit.PickiT;
//import com.hbisoft.pickit.PickiTCallbacks;

import java.io.File;
import java.util.ArrayList;

import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import ir.hesam.rx_java.G;
import ir.hesam.rx_java.module.FileResponse;
import ir.hesam.rx_java.module.FileUtils;
import ir.hesam.rx_java.module.ProgressRequestBody;
import ir.hesam.rx_java.NetWork.API.NetworkLayer;
import ir.hesam.rx_java.R;
import ir.hesam.rx_java.databinding.FragmentUploadBinding;
import ir.hesam.rx_java.second.SecondActivity;
import okhttp3.MultipartBody;

public class UploadFragment extends Fragment {
        //implements PickiTCallbacks , ProgressRequestBody.UploadCallbacks {


    private static final UploadFragment ourInstance = new UploadFragment();
    public static UploadFragment getInstance(){return ourInstance;}


    private static final int FILE_REQ = 859;
    FragmentUploadBinding binding;
    //PickiT pickiT;
    File files;
    ProgressRequestBody fileBody;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_upload, container, false);
        View view = binding.getRoot();  // <-- Binding in Fragment


        /*
         * Pick-T for Activity -->  { pickiT = new PickiT(this, this, this); }
         * Pick-T for Fragment -->  { pickiT = new PickiT(getContext(), this, getActivity());
         */
        //pickiT = new PickiT(getContext(), this, getActivity());



        // Open Activity in Phone For Choose File For Upload
        binding.uploadBtn.setOnClickListener(view1 -> {
            Intent intent = new Intent();
            intent.setAction(Intent.ACTION_GET_CONTENT);
            intent.setType("*/*");
            // startActivityForResult(intent.createChooser(intent , "select file" ));
            intent = Intent.createChooser(intent, "انتخاب فایل");
            startActivityForResult(intent, FILE_REQ);
        });



        // Upload File By SecondActivity
        binding.uploadBtn2.setOnClickListener(view1 -> {
            Intent intent = new Intent(getContext(), SecondActivity.class);
            startActivity(intent);
        });

        return view;  // <-- Important in "Fragment"
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            if (requestCode == FILE_REQ )

                if (data != null) {
                    Uri uri = data.getData();  // <-- Make Uri
                    Log.e("uri: " , uri.getPath());
                    binding.uriTxt.setText(uri.getPath());
                }
           // pickiT.getPath(data.getData(), Build.VERSION.SDK_INT);  // <-- Uri To Path

        }
    }




//    @Override
//    public void PickiTonUriReturned() {
//
//    }
//
//    @Override
//    public void PickiTonStartListener() {
//
//    }
//
//    @Override
//    public void PickiTonProgressUpdate(int progress) {
//
//    }
//
//    @Override
//    public void PickiTonCompleteListener(String path, boolean wasDriveFile, boolean wasUnknownProvider, boolean wasSuccessful, String Reason) {
//
//        this.files = new File(path); // <-- get path
//        binding.pathTxt.setText(path);
//        uploadFile();   //   <-- Call Function for Final Upload
//
//    }
//
//    @Override
//    public void PickiTonMultipleCompleteListener(ArrayList<String> paths, boolean wasSuccessful, String Reason) {
//
//    }
//
//    // The Most Important Function for Upload
//    // Call Api
//    // Call RequestBody
//    private void uploadFile() {
//        MultipartBody.Part body = null;
//
//        if (files != null) {
//            fileBody = new ProgressRequestBody(files, files.getName(),  this);
//            body = MultipartBody.Part.createFormData("file", FileUtils.file_name(files), fileBody);
//        }
//
//        NetworkLayer.getInstance().new_file(G.getToken() , body)
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribeOn(Schedulers.io())
//                .subscribe(new SingleObserver<FileResponse>() {
//                    @Override
//                    public void onSubscribe(@NonNull Disposable d) {
//
//                    }
//
//                    @Override
//                    public void onSuccess(@NonNull FileResponse fileResponse) {
//                        Toast.makeText(getActivity(), "Uploaded", Toast.LENGTH_SHORT).show();
//                        Log.e("succes" , "uploaded");
//                    }
//
//                    @Override
//                    public void onError(@NonNull Throwable e) {
//                        Log.e("error" , "not uploaded");
//
//                    }
//                });
//    }
//
//
//    @Override
//    public void onProgressUpdate(int percentage) {
//
//    }
//
//    @Override
//    public void onError() {
//
//    }
//
//    @Override
//    public void onFinish() {
//
//    }
}
