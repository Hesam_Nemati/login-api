package ir.hesam.rx_java.Notification;

import static ir.hesam.rx_java.Notification.App.CHANNEL_ID;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import android.app.Notification;
import android.app.NotificationManager;
import android.os.Bundle;
import android.widget.Button;

import ir.hesam.rx_java.R;

public class MainActivity extends AppCompatActivity {

    private NotificationManagerCompat notificationManager;
    Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        notificationManager = NotificationManagerCompat.from(this);
        button=findViewById(R.id.button);

        button.setOnClickListener(view -> {showNotification();});
    }

    public void showNotification(){
        Notification notification = new NotificationCompat.Builder(this , CHANNEL_ID)
                .setSmallIcon(R.drawable.cheque_icon)
                .setContentTitle("Tittle")
                .setContentText("This is a default notification")
                .build();

        notificationManager.notify(1 , notification);
    }
}