package ir.hesam.rx_java.second;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import ir.hesam.rx_java.module.ReponseBody.ReasonAnnounce;
import ir.hesam.rx_java.R;
import ir.hesam.rx_java.databinding.ItemBaseBinding;

public class ElanatAdapter extends RecyclerView.Adapter<ElanatAdapter.VersionViewHolder> {
    Context context;

    ArrayList <ReasonAnnounce>  elanats = new ArrayList<>();

    public ElanatAdapter(Context context, ArrayList <ReasonAnnounce>  elanats) {
        this.context = context;

        this.elanats = elanats;

        Log.e("adapter", elanats.toString());
    }


    @Override
    public VersionViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_base, parent, false);
        ElanatAdapter.VersionViewHolder viewHolder_Elanat = new ElanatAdapter.VersionViewHolder(view);

        return viewHolder_Elanat;
    }

    @Override
    public void onBindViewHolder(final VersionViewHolder holder, final int position) {

        //position -> current item
        holder.binding.elanatText.setText(elanats.get(position).getText());
        holder.binding.title.setText(elanats.get(position).getTitle());
        holder.binding.time.setText(elanats.get(position).getDate());


    }

    @Override
    public int getItemCount() {
        return elanats == null ? 0 : elanats.size();
    }

    public class VersionViewHolder extends RecyclerView.ViewHolder {

        ItemBaseBinding binding;

        public VersionViewHolder(View itemView) {
            super(itemView);
            binding = ItemBaseBinding.bind(itemView);
        }
    }


}
