package ir.hesam.rx_java.second;


import android.content.Intent;
import android.os.Build;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.LinearLayoutManager;

import java.util.ArrayList;

import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import ir.hesam.rx_java.G;
import ir.hesam.rx_java.module.FileResponse;
import ir.hesam.rx_java.module.FileUtils;
import ir.hesam.rx_java.module.ProgressRequestBody;
import ir.hesam.rx_java.module.ReponseBody.AnnounceList;
import ir.hesam.rx_java.module.ReponseBody.ReasonAnnounce;
import ir.hesam.rx_java.NetWork.API.NetworkLayer;
import okhttp3.MultipartBody;


@RequiresApi(api = Build.VERSION_CODES.LOLLIPOP_MR1)
public class SecondActivityPresenter {


    private static SecondActivityPresenter ourInstance;
    public static SecondActivityPresenter getInstance(){
        if (ourInstance == null){
            ourInstance = new SecondActivityPresenter();
        }
        return ourInstance;
    }



    public void setLayoutManager(SecondActivity activity,ArrayList<ReasonAnnounce> list){

        LinearLayoutManager layoutManager
                = new LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false);
        activity.binding.recycler.setLayoutManager(layoutManager);

        ElanatAdapter elanatAdapter;
        elanatAdapter = new ElanatAdapter(activity, list);
        activity.binding.recycler.setAdapter(elanatAdapter);
        elanatAdapter.notifyDataSetChanged();

    }

    public void get_announce(SecondActivity activity ){
        NetworkLayer.getInstance().get_announce(G.getToken())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<AnnounceList>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {

                    }

                    @Override
                    public void onSuccess(@NonNull AnnounceList announceList) {

                        setLayoutManager(activity, announceList.getAnnounces());

                    }
                    @Override
                    public void onError(@NonNull Throwable e) {
                    }
                });
    }

   /*
   *  pickFile for go to the choose file in your phone.:/
   */
    public void pickFile(SecondActivity activity){
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_GET_CONTENT);
        intent.setType("*/*");
        intent = Intent.createChooser(intent, "انتخاب فایل");
        activity.startActivityForResult(intent, activity.FILE_REQ);
    }


    /*
     *  uploadFile for request to Api and upload.:/
     */
    public void uploadFile(SecondActivity activity){

        MultipartBody.Part body = null;

        if (activity.file != null) {
            activity.fileBody = new ProgressRequestBody(activity.file, activity.file.getName(), (ProgressRequestBody.UploadCallbacks) activity);
            body = MultipartBody.Part.createFormData("file", FileUtils.file_name(activity.file), activity.fileBody);
        }

        NetworkLayer.getInstance().new_file(G.getToken() , body)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<FileResponse>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {

                    }

                    @Override
                    public void onSuccess(@NonNull FileResponse fileResponse) {

                        Toast.makeText(activity, "Uploaded", Toast.LENGTH_SHORT).show();
                        Log.e("succes" , "uploaded");
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        Log.e("error" , "not uploaded");

                    }
                });

    }




}
