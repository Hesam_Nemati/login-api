package ir.hesam.rx_java.second;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;

//import com.hbisoft.pickit.PickiT;
//import com.hbisoft.pickit.PickiTCallbacks;

import java.io.File;
import java.util.ArrayList;

import ir.hesam.rx_java.module.Elanat;
import ir.hesam.rx_java.module.ProgressRequestBody;
import ir.hesam.rx_java.R;
import ir.hesam.rx_java.databinding.ActivitySecondBinding;

public class SecondActivity extends AppCompatActivity {
        //implements PickiTCallbacks, ProgressRequestBody.UploadCallbacks {

    ArrayList<Elanat> elanats;


    /*
     *  these arrayList for upload more than one file.:/
     *
    ArrayList<FileUploader> files = new ArrayList<>();
    ArrayList<FileUploader> files_upload = new ArrayList<>();
    */

    File file;
    ProgressRequestBody fileBody;



    ActivitySecondBinding binding;
     static final int FILE_REQ = 859; // Result code for "startActivityForResult"


     //PickiT pickiT;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP_MR1)  // new added
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_second);

        //pickiT = new PickiT(this , this , this); //for pickT library


        SecondActivityPresenter.getInstance().get_announce(this);

        binding.uploadButton.setOnClickListener(v->{
            SecondActivityPresenter.getInstance().pickFile(this);

        });

    }

     // this is convertor for Uri to Path (Uri --> path)
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

            if (resultCode == RESULT_OK) {
                if (requestCode == FILE_REQ )

                    if (data != null) {
                        Uri uri = data.getData();
                        Log.e("uri: " , uri.getPath());
                    }
                //pickiT.getPath(data.getData(), Build.VERSION.SDK_INT);

            }
        }


//    @Override
//    public void PickiTonUriReturned() {
//    }
//
//    @Override
//    public void PickiTonStartListener() {
//    }
//
//    @Override
//    public void PickiTonProgressUpdate(int progress) {
//    }
//
//    // here we can use Path by PickT library for upload ------
//    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP_MR1)
//    @Override
//    public void PickiTonCompleteListener(String path, boolean wasDriveFile, boolean wasUnknownProvider, boolean wasSuccessful, String Reason) {
//        Log.e("path: " , path);
//
//       /*
//        *  these for upload more than one file.:/
//        *
//        String id = UUID.randomUUID().toString();
//        FileUploader file = new FileUploader(path,id);
//        files.add(file);
//        files_upload.add(file);
//        */
//        this.file = new File(path);
//        SecondActivityPresenter.getInstance().uploadFile(this);
//
//
//
//    }
//
//
//
//
//    @Override
//    public void PickiTonMultipleCompleteListener(ArrayList<String> paths, boolean wasSuccessful, String Reason) {
//
//    }
//
//    @Override
//    public void onProgressUpdate(int percentage) {
//
//    }
//
//    @Override
//    public void onError() {
//
//    }
//
//    @Override
//    public void onFinish() {
//
//    }
}