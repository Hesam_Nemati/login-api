package ir.hesam.rx_java;

import android.content.Context;
import android.content.SharedPreferences;

import androidx.multidex.MultiDexApplication;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class G extends MultiDexApplication {

    public static Context context;
    private static SharedPreferences sharedPref;
    private static String token;
    private static String name;
    private static String type;
    private static String username;

    public static Context getContext() {
        return context;
    }



    public static String getName() {
        getFromPref();
        return name;
    }

    public static String getType() {
        getFromPref();
        return type;
    }

    public static String getUsername() {
        getFromPref();
        return username;
    }

    public static Context getGetContentResolvercontext() {
        return getContentResolvercontext;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        context = getApplicationContext();
    }

    public static String getToken() {
        getFromPref();
        return "Bearer "+token;
    }
    public static String getPureToken() {
        getFromPref();
        return token;
    }

    public static void getFromPref() {
        if (context == null) {

        }
        sharedPref = context.getSharedPreferences("Hamiss", context.MODE_PRIVATE);
        token = sharedPref.getString("TOKEN", null);
        name =  sharedPref.getString("NAME" , null);
        type =  sharedPref.getString("TYPE" , null);
        username =  sharedPref.getString("USERNAME" , null);


    }

    public static void add_to_sharedPref(String token ,String name , String type, String username){
        sharedPref = context.getSharedPreferences("Hamiss", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString("TOKEN", token);
        editor.putString("NAME", name);
        editor.putString("TYPE", type);
        editor.putString("USERNAME", username);

        editor.apply();
    }
    public static Context getContentResolvercontext;


    public static RecyclerView default_recycle_conf_grid(RecyclerView recycle, int count) {

        GridLayoutManager manager = new GridLayoutManager(context, count, RecyclerView.VERTICAL, false);
        recycle.setLayoutManager(manager);
        return recycle;
    }
    public static RecyclerView default_recycle_conf(RecyclerView recycle) {

        LinearLayoutManager layoutManager = new LinearLayoutManager(context, RecyclerView.VERTICAL, false);
        recycle.setLayoutManager(layoutManager);
        return recycle;
    }


    public SharedPreferences getSharedPref(){return sharedPref;}

}
