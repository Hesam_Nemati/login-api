package ir.madtalk.android.cheque.netWork;

import java.util.ArrayList;

import io.reactivex.Single;
import ir.madtalk.android.cheque.module.ChequeList;
import ir.madtalk.android.cheque.module.ChqueResult;
import ir.madtalk.android.cheque.module.CreatCheque;
import ir.madtalk.android.cheque.module.SearchAccount;
import okhttp3.MultipartBody;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;

public class NetworkLayer implements ApiClient{
    private ApiClient apiClient;
    private static final NetworkLayer ourInstance = new NetworkLayer();

    public static NetworkLayer getInstance() {
        return ourInstance;
    }

    public NetworkLayer() {
        apiClient = ApiService.getApiClient();
    }

    @Override
    public Single<ChequeList> get_cheque_list(String token) {
        return apiClient.get_cheque_list(token);
    }

    @Override
    public Single<ChqueResult> get_detail_cheque(String token , int id) {
        return apiClient.get_detail_cheque(token , id);
    }

    @Override
    public Single<CreatCheque> creat_cheque(String token , int student_id , String for_what , String payـto , String exporter ,
                                            String bank , int amount , int state , boolean sayad) {

        return apiClient.creat_cheque(token , student_id , for_what , payـto , exporter , bank , amount , state , sayad );
    }


    @Override
    public Single<ArrayList<SearchAccount>> search_account(String token , String type , String word) {
        return apiClient.search_account(token , type , word);
    }


}
