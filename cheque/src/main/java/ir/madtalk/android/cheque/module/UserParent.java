package ir.madtalk.android.cheque.module;

import com.google.gson.annotations.SerializedName;

public class UserParent {

    @SerializedName("id")
    private int id;

    @SerializedName("user")
    private user_parent user_parent = new user_parent();

    @SerializedName("first_name")
    private String first_name;

    @SerializedName("last_name")
    private String last_name;

    @SerializedName("mobile_phone")
    private String mobile_phone;

    @SerializedName("national_id")
    private String national_id;

    @SerializedName("birth_date")
    private String birth_date;

    @SerializedName("profile_pic")
    private String profile_pic;

    @SerializedName("school")
    private int school;



    public int getId() {
        return id;
    }

    public ir.madtalk.android.cheque.module.user_parent getUser_parent() {
        return user_parent;
    }

    public String getFirst_name() {
        return first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public String getMobile_phone() {
        return mobile_phone;
    }

    public String getNational_id() {
        return national_id;
    }

    public String getBirth_date() {
        return birth_date;
    }

    public String getProfile_pic() {
        return profile_pic;
    }

    public int getSchool() {
        return school;
    }
}
