package ir.madtalk.android.cheque.Cheque;

import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import io.reactivex.Scheduler;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import ir.madtalk.android.cheque.G;
import ir.madtalk.android.cheque.module.Cheque;
import ir.madtalk.android.cheque.module.ChequeList;
import ir.madtalk.android.cheque.netWork.NetworkLayer;


public class ChequePresenter {

    private static ChequePresenter presenter;

    public static ChequePresenter getInstance() {
        if (presenter == null)
            presenter = new ChequePresenter();
        return presenter;
    }


    public void get_list(ChequeListActivity activity){
        NetworkLayer.getInstance().get_cheque_list(G.getToken())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new SingleObserver<ChequeList>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {

                    }

                    @Override
                    public void onSuccess(@NonNull ChequeList chequeList) {

                        activity.binding.progress.progressRltv.setVisibility(View.INVISIBLE);
                        activity.binding.chequeRecycler.setVisibility(View.VISIBLE);
                        activity.binding.newChequeBtn.setVisibility(View.VISIBLE);

                        activity.binding.chequeRecycler.setAdapter(activity.adapter);
                        activity.binding.chequeRecycler.setLayoutManager(new LinearLayoutManager(activity, RecyclerView.VERTICAL, true));
                        activity.adapter.versionModels.accept(chequeList.getChque_result());
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        e.printStackTrace();
                    }
                });

    }

}
