package ir.madtalk.android.cheque.Dynamic;

import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;

import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import ir.madtalk.android.cheque.G;
import ir.madtalk.android.cheque.module.ChqueResult;
import ir.madtalk.android.cheque.netWork.NetworkLayer;

public class SingleChequePresenter {

    private static SingleChequePresenter presenter;

    public static SingleChequePresenter getInstance() {
        if (presenter == null)
            presenter = new SingleChequePresenter();
        return presenter;
    }

    public void init_layout(SingleChequeActivity activity, int margin){

        NetworkLayer.getInstance().get_detail_cheque(G.getToken() , activity.id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<ChqueResult>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {

                    }

                    @Override
                    public void onSuccess(@NonNull ChqueResult chqueResult) {

                        String name = chqueResult.getStudent_obj().getUserStudent().getFirst_name() + " " + chqueResult.getStudent_obj().getUserStudent().getLast_name();
                        activity.nameTv.setText(name);
                        activity.innerLinear.addView(activity.nameTv);
                        activity.innerLinear.addView(Views.getInstance().sc_items_ll(activity, "بابت: ", chqueResult.getFor_what(), margin));
                        activity.innerLinear.addView(Views.getInstance().sc_items_ll(activity, "در وجه: ", chqueResult.getPayـto(), margin));
                        activity.innerLinear.addView(Views.getInstance().sc_items_ll(activity, "مبلغ: ", chqueResult.getAmount(), margin));
                        activity.innerLinear.addView(Views.getInstance().sc_items_ll(activity, "موعد پرداخت: ", chqueResult.getDate(), margin));
                        activity.innerLinear.addView(Views.getInstance().sc_items_ll(activity, "وضعیت: ", chqueResult.getGet_setState().get(chqueResult.getState()), margin));
                        activity.statusImg.setBackgroundColor(activity.getResources().getColor(chqueResult.getSet_cadr_color().get(chqueResult.getState())));

                        Views.getInstance().simple_toolbar(activity, activity.toolbar, name, margin);

//                        activity.name = name;

                        activity.toolbar.setId(View.generateViewId());

                        String amount = chqueResult.getAmount();

//                        Log.e("TAG", "amount: " + String.format("%,.0f", amount));

                        activity.cardView.setVisibility(View.VISIBLE);
                        activity.cardParams.addRule(RelativeLayout.BELOW, activity.toolbar.getId());
                        activity.progressIndicator.setVisibility(View.INVISIBLE);

                        activity.mainRelative.addView(activity.toolbar);
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        e.printStackTrace();
                    }
                });
    }

}
