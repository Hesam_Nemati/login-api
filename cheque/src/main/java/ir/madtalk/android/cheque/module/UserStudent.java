package ir.madtalk.android.cheque.module;

import com.google.gson.annotations.SerializedName;

public class UserStudent {

    @SerializedName("username")
    private String username;

    @SerializedName("first_name")
    private String first_name;

    @SerializedName("last_name")
    private String last_name;

    @SerializedName("type")
    private String type;

    @SerializedName("school")
    private int school;

    @SerializedName("id")
    private int id;

    public String getUsername() {
        return username;
    }

    public String getFirst_name() {
        return first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public String getType() {
        return type;
    }

    public int getSchool() {
        return school;
    }

    public int getId() {
        return id;
    }
}
