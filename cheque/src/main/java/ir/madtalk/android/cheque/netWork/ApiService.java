package ir.madtalk.android.cheque.netWork;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiService {
    private static ApiClient apiClient = null;
    private static boolean check;

    public ApiService() {
    }

    public static ApiClient getApiClient() {

        if (apiClient == null) {



            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
//              set your desired log level
            logging.setLevel(HttpLoggingInterceptor.Level.BODY);


            OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
//              add your other interceptors …

//               add logging as last interceptor
            httpClient.addInterceptor(logging);  // <-- this is the important line!


            httpClient.connectTimeout(400, TimeUnit.SECONDS)
                    .readTimeout(600, TimeUnit.SECONDS)
                    .writeTimeout(600, TimeUnit.SECONDS);


            Retrofit.Builder builder = new Retrofit.Builder();
            builder
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(httpClient.build())
                    .baseUrl("https://api2.madtalk.ir/api/");

            Retrofit retrofit = builder.build();
            apiClient = retrofit.create(ApiClient.class);
        }
        return apiClient;
    }
}
