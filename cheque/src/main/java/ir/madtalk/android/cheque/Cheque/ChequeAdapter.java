package ir.madtalk.android.cheque.Cheque;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.jakewharton.rxrelay2.BehaviorRelay;

import java.util.ArrayList;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import ir.madtalk.android.cheque.R;
import ir.madtalk.android.cheque.SingleCheque.SingleChequeActivity;
import ir.madtalk.android.cheque.databinding.ChequeItemBaseBinding;
import ir.madtalk.android.cheque.module.Cheque;
import ir.madtalk.android.cheque.module.ChequeList;
import ir.madtalk.android.cheque.module.ChqueResult;


public class ChequeAdapter extends RecyclerView.Adapter<ChequeAdapter.VersionViewHolder> {
    Context context;
    CompositeDisposable bag = new CompositeDisposable();
    private int width;
    public BehaviorRelay<ArrayList<ChqueResult>> versionModels = BehaviorRelay.createDefault(new ArrayList<>());

    {
        versionModels.observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<ArrayList<ChqueResult>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        bag.add(d);
                    }

                    @Override
                    public void onNext(ArrayList<ChqueResult> chequeList) {

                        notifyDataSetChanged();
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }


    public ChequeAdapter(Context context) {
        this.context = context;

    }


    @Override
    public VersionViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cheque_item_base, parent, false);
        VersionViewHolder viewHolder = new VersionViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final VersionViewHolder holder, final int position) {


        ChqueResult cheque = versionModels.getValue().get(position);
        holder.binding.chequeAmountTv.setText(versionModels.getValue().get(position).getAmount());
        holder.binding.chequeDescriptionTv.setText(versionModels.getValue().get(position).getPayـto());
        holder.binding.chequeOwnerTv.setText(versionModels.getValue().get(position).getStudent_obj().getUserStudent().getFirst_name() +
                " " + versionModels.getValue().get(position).getStudent_obj().getUserStudent().getLast_name());




    }


    @Override
    public int getItemCount() {
        return versionModels == null ? 0 : versionModels.getValue().size();
    }

    class VersionViewHolder extends RecyclerView.ViewHolder {

        ChequeItemBaseBinding binding;


        public VersionViewHolder(View itemView) {
            super(itemView);
            binding = ChequeItemBaseBinding.bind(itemView);


            binding.detailsBtn.setOnClickListener(view -> {
                intent();
            });

            binding.chequeCard.setOnClickListener(view -> {
                intent();
            });


        }


        public void intent(){
            Intent intent = new Intent(context, SingleChequeActivity.class);
            intent.putExtra("cheque_id", versionModels.getValue().get(getAdapterPosition()).getId());
            context.startActivity(intent);
        }

    }


}
