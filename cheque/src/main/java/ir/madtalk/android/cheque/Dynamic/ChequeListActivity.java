package ir.madtalk.android.cheque.Dynamic;

import static android.view.View.generateViewId;
import static android.view.ViewGroup.LayoutParams.MATCH_PARENT;
import static android.view.ViewGroup.LayoutParams.WRAP_CONTENT;
import static android.widget.RelativeLayout.ALIGN_BOTTOM;
import static android.widget.RelativeLayout.ALIGN_LEFT;
import static android.widget.RelativeLayout.ALIGN_PARENT_BOTTOM;
import static android.widget.RelativeLayout.ALIGN_RIGHT;
import static android.widget.RelativeLayout.ALIGN_TOP;
import static android.widget.RelativeLayout.CENTER_IN_PARENT;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.appbar.MaterialToolbar;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton;
import com.google.android.material.progressindicator.CircularProgressIndicator;
import com.google.android.material.textview.MaterialTextView;

import de.hdodenhof.circleimageview.CircleImageView;
import ir.madtalk.android.cheque.G;
import ir.madtalk.android.cheque.NewCheque.NewChequeActivity;
import ir.madtalk.android.cheque.R;

public class ChequeListActivity extends AppCompatActivity {


    /**
     * In this Activity show list of all cheques
     * If token Is a Manger of school can see all of cheque or
     * Its be Student can see own cheques --> The "addChequeBtn" Button is Should be INVISIBLE
     */

    ChequeLstAdapter adapter;

    MaterialToolbar toolbar;
    RecyclerView chequeRecycler;
    RelativeLayout mainLayout;
    CircularProgressIndicator progressIndicator;
    ExtendedFloatingActionButton addChequeBtn;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        adapter = new ChequeLstAdapter(this);
        progressIndicator = new CircularProgressIndicator(this);
        toolbar = new MaterialToolbar(this);
        mainLayout = new RelativeLayout(this);
        chequeRecycler = new RecyclerView(this);
        addChequeBtn = new ExtendedFloatingActionButton(this);


        RelativeLayout.LayoutParams mainLayoutParam = new RelativeLayout.LayoutParams(MATCH_PARENT, MATCH_PARENT);
        mainLayout.setLayoutParams(mainLayoutParam);
        mainLayout.setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
        mainLayout.animate();
        mainLayout.setBackgroundColor(getResources().getColor(R.color.white_light));

        Views.getInstance().simple_toolbar(this, toolbar, "لیست چک ها", 20);
        toolbar.setId(View.generateViewId());

        RelativeLayout.LayoutParams chequeRecyclerParams = new RelativeLayout.LayoutParams(MATCH_PARENT, WRAP_CONTENT);
        chequeRecyclerParams.addRule(RelativeLayout.BELOW, toolbar.getId());
        chequeRecycler.setLayoutParams(chequeRecyclerParams);
        chequeRecycler.setVisibility(View.INVISIBLE);

        RelativeLayout.LayoutParams btnParams = new RelativeLayout.LayoutParams(WRAP_CONTENT, WRAP_CONTENT);
        btnParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
        btnParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
        btnParams.setMargins(0, 0, 30, 30);
        addChequeBtn.setText(getResources().getText(R.string.new_cheque));
        addChequeBtn.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
        addChequeBtn.setIcon(getResources().getDrawable(R.drawable.ic_action_add_green));
        addChequeBtn.setId(View.generateViewId());
        addChequeBtn.setLayoutParams(btnParams);
        addChequeBtn.setTextColor(getResources().getColor(R.color.white));
        addChequeBtn.setVisibility(View.INVISIBLE);
        addChequeBtn.setAnimateShowBeforeLayout(true);

        // If Type is student , addButton should INVISIBLE
//        if (G.is_student()){
//            addChequeBtn.setVisibility(View.INVISIBLE);
//        }

        addChequeBtn.setOnClickListener(view -> {
            startActivity(new Intent(this, NewChequeActivity.class));
        });


        chequeRecycler.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                if (newState == 0) {
                    addChequeBtn.setVisibility(View.VISIBLE);
                } else {
                    addChequeBtn.setVisibility(View.INVISIBLE);
                }

            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
            }
        });


        RelativeLayout.LayoutParams progressParams = new RelativeLayout.LayoutParams(WRAP_CONTENT, WRAP_CONTENT);
        progressParams.addRule(RelativeLayout.CENTER_IN_PARENT);
        progressIndicator.setLayoutParams(progressParams);
        progressIndicator.setIndeterminate(true);
        progressIndicator.setIndicatorColor(getResources().getColor(R.color.colorPrimaryDark));
        progressIndicator.setVisibility(View.VISIBLE);
        progressIndicator.setId(View.generateViewId());

        mainLayout.addView(toolbar);
        mainLayout.addView(progressIndicator);
        mainLayout.addView(addChequeBtn);
        mainLayout.addView(chequeRecycler);

        ChequeListPresenter.getInstance().get_list(this);

        setContentView(mainLayout);

    }
}
