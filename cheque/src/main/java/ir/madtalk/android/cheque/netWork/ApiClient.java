package ir.madtalk.android.cheque.netWork;

import java.util.ArrayList;

import io.reactivex.Single;
import ir.madtalk.android.cheque.module.ChequeList;
import ir.madtalk.android.cheque.module.ChqueResult;
import ir.madtalk.android.cheque.module.CreatCheque;
import ir.madtalk.android.cheque.module.SearchAccount;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface ApiClient {

    @GET("v1.0.0/financial/cheque/list/")
    Single <ChequeList> get_cheque_list(@Header("Authorization") String token);

    @GET("v1.0.0/financial/cheque/detail/{id}/")
    Single<ChqueResult> get_detail_cheque(@Header("Authorization") String token , @Path("id") int id);

    @FormUrlEncoded
    @POST("v1.0.0/financial/cheque/list/")
    Single<CreatCheque> creat_cheque(@Header("Authorization") String token , @Field("student") int student_id , @Field("for_what") String for_what,
                                     @Field("payـto") String payـto , @Field("exporter") String exporter , @Field("bank") String bank ,
                                     @Field("amount") int amount , @Field("state") int state , @Field("sayad") boolean sayad);

    @FormUrlEncoded
    @POST("v1.0.0/account/search/")
    Single <ArrayList<SearchAccount>> search_account(@Header("Authorization") String token , @Field("type") String type , @Field("word") String word);
}
