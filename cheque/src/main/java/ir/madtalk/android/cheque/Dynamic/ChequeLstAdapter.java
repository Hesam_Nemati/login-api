package ir.madtalk.android.cheque.Dynamic;

import static android.view.ViewGroup.LayoutParams.MATCH_PARENT;
import static android.view.ViewGroup.LayoutParams.WRAP_CONTENT;
import static android.widget.RelativeLayout.ALIGN_PARENT_LEFT;
import static android.widget.RelativeLayout.ALIGN_PARENT_RIGHT;
import static android.widget.RelativeLayout.ALIGN_RIGHT;
import static android.widget.RelativeLayout.BELOW;
import static android.widget.RelativeLayout.CENTER_VERTICAL;
import static android.widget.RelativeLayout.LEFT_OF;
import static android.widget.RelativeLayout.RIGHT_OF;

import android.content.Intent;
import android.graphics.drawable.GradientDrawable;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.textview.MaterialTextView;
import com.jakewharton.rxrelay2.BehaviorRelay;

import java.util.ArrayList;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import ir.madtalk.android.cheque.R;
import ir.madtalk.android.cheque.module.ChqueResult;

public class ChequeLstAdapter extends RecyclerView.Adapter<ChequeLstAdapter.VersionViewHolder> {

    ChequeListActivity activity;

    CompositeDisposable bag = new CompositeDisposable();
    BehaviorRelay<ArrayList<ChqueResult>> versionModels = BehaviorRelay.createDefault(new ArrayList<>());

    {
        versionModels.observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<ArrayList<ChqueResult>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        bag.add(d);
                    }

                    @Override
                    public void onNext(ArrayList<ChqueResult> chequeList) {

                        notifyDataSetChanged();
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }


    public ChequeLstAdapter(ChequeListActivity context) {
        this.activity = context;

    }


    @Override
    public ChequeLstAdapter.VersionViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        CardView cardView = new CardView(activity);
        RelativeLayout relativeLayout = new RelativeLayout(activity);
        ImageView icon_iv = new ImageView(activity);
        MaterialTextView nameTv = new MaterialTextView(activity);
        MaterialTextView amountTv = new MaterialTextView(activity);
        MaterialTextView descriptionTv = new MaterialTextView(activity);
        MaterialButton detailBtn = new MaterialButton(activity);


        CardView.LayoutParams card_param = new FrameLayout.LayoutParams(MATCH_PARENT, WRAP_CONTENT);
        card_param.setMargins(20,10,20,10);
        cardView.setLayoutParams(card_param);
        GradientDrawable shape = new GradientDrawable();
        shape.setCornerRadius(20);
        cardView.setBackground(shape);


        RelativeLayout.LayoutParams relative_param = new RelativeLayout.LayoutParams(MATCH_PARENT, WRAP_CONTENT);
        relativeLayout.setLayoutParams(relative_param);
        relativeLayout.setBackgroundColor(activity.getResources().getColor(R.color.white));


        RelativeLayout.LayoutParams icon_param = new RelativeLayout.LayoutParams(WRAP_CONTENT, WRAP_CONTENT);
        icon_iv.setLayoutParams(icon_param);
        icon_iv.setImageResource(R.drawable.cheque_icon);
        icon_iv.setId(View.generateViewId());
        icon_param.addRule(ALIGN_PARENT_RIGHT);
        icon_param.addRule(CENTER_VERTICAL);
        icon_param.rightMargin = 0;
        icon_param.setMargins(10, 0, 30, 0);


        RelativeLayout.LayoutParams name_tv_param = new RelativeLayout.LayoutParams(WRAP_CONTENT, WRAP_CONTENT);
        nameTv.setId(View.generateViewId());
        name_tv_param.addRule(LEFT_OF, icon_iv.getId());
        name_tv_param.addRule(ALIGN_RIGHT, amountTv.getId());
        nameTv.setGravity(Gravity.RIGHT);
        nameTv.setText("بهراد جعفریبهراد جعفریبهراد جعفریبهراد جعفریبهراد جعفریبهراد جعفریبهراد جعفریبهراد جعفری");
        nameTv.setTextSize(15);
        nameTv.setTextColor(activity.getResources().getColor(R.color.black));
        name_tv_param.setMargins(10, 10, 30, 0);

        RelativeLayout.LayoutParams descriptionTvParam = new RelativeLayout.LayoutParams(WRAP_CONTENT, WRAP_CONTENT);
        descriptionTv.setId(View.generateViewId());
        descriptionTvParam.addRule(BELOW, nameTv.getId());
        descriptionTvParam.addRule(LEFT_OF, icon_iv.getId());
        descriptionTv.setGravity(Gravity.RIGHT);
        descriptionTv.setText("بابت شهریه آبانماه");
        descriptionTvParam.setMargins(15, 30, 30, 0);


        RelativeLayout.LayoutParams amountTvParam = new RelativeLayout.LayoutParams(WRAP_CONTENT, WRAP_CONTENT);
        amountTv.setId(View.generateViewId());
        amountTv.setGravity(Gravity.LEFT);
        amountTvParam.addRule(RIGHT_OF, nameTv.getId());
        amountTvParam.addRule(ALIGN_RIGHT, nameTv.getId());
        amountTvParam.addRule(ALIGN_PARENT_LEFT);
        amountTvParam.setMargins(30, 10, 10, 0);


        RelativeLayout.LayoutParams detailBtnParam = new RelativeLayout.LayoutParams(WRAP_CONTENT, WRAP_CONTENT);
        detailBtn.setId(View.generateViewId());
        detailBtnParam.addRule(ALIGN_PARENT_LEFT);
        detailBtnParam.addRule(BELOW, amountTv.getId());
        detailBtnParam.leftMargin = 30;
        detailBtn.setText(activity.getText(R.string.details));
        detailBtn.setBackgroundColor(activity.getResources().getColor(R.color.colorPrimaryDark));




        relativeLayout.addView(icon_iv, icon_param);
        relativeLayout.addView(nameTv, name_tv_param);
        relativeLayout.addView(descriptionTv, descriptionTvParam);
        relativeLayout.addView(amountTv, amountTvParam);
        relativeLayout.addView(detailBtn, detailBtnParam);
        cardView.addView(relativeLayout);

//        parent.addView(cardView);

        return new VersionViewHolder(cardView, icon_iv, nameTv, descriptionTv, detailBtn, amountTv);


    }

    @Override
    public void onBindViewHolder(final ChequeLstAdapter.VersionViewHolder holder, final int position) {


        ChqueResult cheque = versionModels.getValue().get(position);
//        holder.binding.chequeAmountTv.setText(versionModels.getValue().get(position).getAmount());
//        holder.binding.chequeDescriptionTv.setText(versionModels.getValue().get(position).getPayـto());
//        holder.binding.chequeOwnerTv.setText(versionModels.getValue().get(position).getStudent_obj().getUserStudent().getFirst_name() +
//                " " + versionModels.getValue().get(position).getStudent_obj().getUserStudent().getLast_name());

        String name = cheque.getStudent_obj().getUserStudent().getFirst_name() + " " + cheque.getStudent_obj().getUserStudent().getLast_name();

        holder.nameTv.setText(name);
        holder.amountTv.setText(cheque.getAmount());


    }


    @Override
    public int getItemCount() {
        return versionModels == null ? 0 : versionModels.getValue().size();
    }

    public class VersionViewHolder extends RecyclerView.ViewHolder {

        CardView cardView;
        ImageView icon_iv;
        MaterialTextView nameTv;
        MaterialTextView descriptionTv;
        MaterialButton detailBtn;
        MaterialTextView amountTv;

        public VersionViewHolder(CardView cardView,
                                 ImageView imageView,
                                 MaterialTextView name_tv,
                                 MaterialTextView descriptionTv,
                                 MaterialButton detail_btn,
                                 MaterialTextView amountTv) {

            super(cardView);

            this.cardView = cardView;
            this.icon_iv = imageView;
            this.nameTv = name_tv;
            this.descriptionTv = descriptionTv;
            this.detailBtn = detail_btn;
            this.amountTv = amountTv;

//            binding = ChequeItemBaseBinding.bind(itemView);
//
//
            detailBtn.setOnClickListener(view -> {
                intent();
            });
//
            cardView.setOnClickListener(view -> {
                intent();
            });


        }


        public void intent() {
            Intent intent = new Intent(activity, SingleChequeActivity.class);
            intent.putExtra("cheque_id", versionModels.getValue().get(getAdapterPosition()).getId());
            activity.startActivity(intent);
        }

    }

}
