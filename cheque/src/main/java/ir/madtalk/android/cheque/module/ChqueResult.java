package ir.madtalk.android.cheque.module;

import com.google.gson.annotations.SerializedName;

import java.util.HashMap;
import java.util.Map;

import ir.madtalk.android.cheque.R;

public class ChqueResult {

    @SerializedName("id")
    private int id;

    @SerializedName("student_obj")
    private Student_obj student_obj = new Student_obj();

    @SerializedName("for_what")
    private String for_what;

    @SerializedName("payـto")
    private String payـto;

    @SerializedName("exporter")
    private String exporter;

    @SerializedName("bank")
    private String bank;

    @SerializedName("amount")
    private Double amount;

    @SerializedName("date")
    private String date;

    @SerializedName("state")
    private int state;

    @SerializedName("sayad")
    private boolean sayad;

    @SerializedName("student")
    private int student;

    @SerializedName("school")
    private int school;


    public int getId() {
        return id;
    }

    public Student_obj getStudent_obj() {
        return student_obj;
    }

    public String getFor_what() {
        return for_what;
    }

    public String getPayـto() {
        return payـto;
    }

    public String getExporter() {
        return exporter;
    }

    public String getBank() {
        return bank;
    }

    public String getAmount() {
        return String.format("%,.0f", amount) + " ریال";
    }

    public String getDate() {
        return date;
    }

    Map<Integer , String> get_setState = new HashMap<Integer , String>();
    Map<Integer , Integer> set_cadr_color = new HashMap<Integer , Integer>();

    public Map<Integer, Integer> getSet_cadr_color() {
        return set_cadr_color;
    }

    public Map<Integer, String> getGet_setState() {
        return get_setState;
    }

    public int getState() {

        get_setState.put(1 , "پرداخت شده");
        get_setState.put(2 , "پرداخت نشده");
        get_setState.put(3 , "برگشت خورده");
        get_setState.put(4 , "پس گرفته شده");

        set_cadr_color.put(1 , R.color.green_font);
        set_cadr_color.put(2 , R.color.red);
        set_cadr_color.put(3 , R.color.red);
        set_cadr_color.put(4 , R.color.gray);


        return state;
    }

    public boolean isSayad() {
        return sayad;
    }

    public int getStudent() {
        return student;
    }

    public int getSchool() {
        return school;
    }
}
