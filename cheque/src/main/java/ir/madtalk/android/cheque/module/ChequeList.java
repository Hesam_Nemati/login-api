package ir.madtalk.android.cheque.module;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class ChequeList {

    @SerializedName("next")
    private String next;

    @SerializedName("previous")
    private String previous;

    @SerializedName("count")
    private String count;

    @SerializedName("page_size")
    private String page_size;

    @SerializedName("result")
    private ArrayList<ChqueResult> chque_result;

    public String getNext() {
        return next;
    }

    public String getPrevious() {
        return previous;
    }

    public String getCount() {
        return count;
    }

    public String getPage_size() {
        return page_size;
    }

    public ArrayList<ChqueResult> getChque_result() {
        return chque_result;
    }
}
