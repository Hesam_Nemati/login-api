package ir.madtalk.android.cheque.module;

import com.google.gson.annotations.SerializedName;

public class Student_obj {

    @SerializedName("id")
    private int id;

    @SerializedName("user")
    private UserStudent userStudent = new UserStudent();

    @SerializedName("parent")
    private UserParent userParent = new UserParent();

    @SerializedName("first_name")
    private String first_name;

    @SerializedName("last_name")
    private String last_name;

    @SerializedName("mobile_phone")
    private String mobile_phone;

    @SerializedName("national_id")
    private String national_id;

    @SerializedName("birth_date")
    private String birth_date;

    @SerializedName("profile_pic")
    private String profile_pic;

    @SerializedName("school")
    private int school;



    public int getId() {
        return id;
    }

    public UserStudent getUserStudent() {
        return userStudent;
    }

    public UserParent getUserParent() {
        return userParent;
    }

    public String getFirst_name() {
        return first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public String getMobile_phone() {
        return mobile_phone;
    }

    public String getNational_id() {
        return national_id;
    }

    public String getBirth_date() {
        return birth_date;
    }

    public String getProfile_pic() {
        return profile_pic;
    }

    public int getSchool() {
        return school;
    }
}
