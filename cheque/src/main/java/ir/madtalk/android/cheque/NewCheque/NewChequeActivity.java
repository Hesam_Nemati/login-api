package ir.madtalk.android.cheque.NewCheque;

import static android.content.ContentValues.TAG;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Toast;

import ir.madtalk.android.cheque.G;
import ir.madtalk.android.cheque.R;
import ir.madtalk.android.cheque.StudentList.StudentListActivity;
import ir.madtalk.android.cheque.Utils;
import ir.madtalk.android.cheque.databinding.ActivityNewChequeBinding;

public class NewChequeActivity extends AppCompatActivity {

    ActivityNewChequeBinding binding;

    private static final int STUDENT = 101;

    private int student_id;
    private String student_name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_new_cheque);

        binding.studentBtn.setOnClickListener(view -> {
            startActivityForResult(new Intent(this, StudentListActivity.class), STUDENT);
        });

        binding.toolbar.nameTv.setText("چک جدید");
        binding.toolbar.attachBtn.setVisibility(View.INVISIBLE);

        binding.toolbar.backBtn.setOnClickListener(view -> {
            onBackPressed();
        });


        binding.toolbar.sendBtn.setOnClickListener(view -> {

            int i = 0;

            if (binding.paidRb.isChecked())
                i = 1;
            else if (binding.notPaidRb.isChecked())
                i = 2;
            else if (binding.bouncedPaidRb.isChecked())
                i = 3;
            else if (binding.returnedRb.isChecked())
                i = 4;

            if (student_id == 0 && i == 0 && binding.forWhatEt.getText().toString().isEmpty()
                    && binding.payToEt.getText().toString().isEmpty()
                    && binding.exporterTv.getText().toString().isEmpty()
                    && binding.bankEt.getText().toString().isEmpty()) {
                Toast.makeText(this, "لطفا موارد مورد نیاز را کامل کنید", Toast.LENGTH_SHORT).show();
            }//                    binding.amountEt.getText().toString().equals(null) &&
            else {
                NewChequePresenter.getInstance().creat_new_cheque(this, G.getToken(),
                        student_id,
                        binding.forWhatEt.getText().toString(),
                        binding.payToEt.getText().toString(),
                        binding.exporterTv.getText().toString(),
                        binding.bankEt.getText().toString(),
                        Integer.parseInt(binding.amountEt.getText().toString()),
                        i,
                        binding.sayadCheck.isChecked()
                );
            }
        });
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == STUDENT) {
            if (resultCode == RESULT_OK) {
                if (data != null) {
                    Bundle extras = data.getExtras();
                    if (extras != null) {

                        student_id = extras.getInt("student_id");
                        student_name = extras.getString("student_name");

                        binding.studentEt.setText(student_name);
                        binding.studentEt.setTextColor(getResources().getColor(R.color.black_light));
                    }
                }
            }
        }
    }
}