package ir.madtalk.android.cheque.SingleCheque;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;

import java.util.HashMap;
import java.util.Map;

import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import ir.madtalk.android.cheque.G;
import ir.madtalk.android.cheque.R;
import ir.madtalk.android.cheque.databinding.ActivitySingleChequeBinding;
import ir.madtalk.android.cheque.module.ChqueResult;
import ir.madtalk.android.cheque.netWork.NetworkLayer;

public class SingleChequeActivity extends AppCompatActivity {

    ActivitySingleChequeBinding binding;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this , R.layout.activity_single_cheque);
        binding.toolbar.attachBtn.setVisibility(View.INVISIBLE);
        binding.toolbar.sendBtn.setVisibility(View.INVISIBLE);

        binding.toolbar.backBtn.setOnClickListener(view -> {
            onBackPressed();
        });



        int id;
        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if (extras == null) {
                id = Integer.parseInt(null);


            } else {
                id = extras.getInt("cheque_id");

            }
        } else {
            id = (Integer) savedInstanceState.getSerializable("cheque_id");

        }

        NetworkLayer.getInstance().get_detail_cheque(G.getToken() , id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<ChqueResult>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {

                    }

                    @Override
                    public void onSuccess(@NonNull ChqueResult chqueResult) {

                        binding.chequeOwnerTv.setText(chqueResult.getStudent_obj().getUserStudent().getFirst_name() + " " + chqueResult.getStudent_obj().getUserStudent().getLast_name());
                        binding.chequeReasonTv.setText(chqueResult.getFor_what());
                        binding.chequeReceiverTv.setText(chqueResult.getPayـto());
                        binding.chequeAmountTv.setText(chqueResult.getAmount());
                        binding.chequeTimeTv.setText(chqueResult.getDate());
                        binding.chequeStatusTv.setText(chqueResult.getGet_setState().get(chqueResult.getState()));
                        binding.color.setBackgroundColor(getResources().getColor(chqueResult.getSet_cadr_color().get(chqueResult.getState())));
                        binding.toolbar.nameTv.setText(chqueResult.getStudent_obj().getUserStudent().getFirst_name() + " " + chqueResult.getStudent_obj().getUserStudent().getLast_name());

                    }

                    @Override
                    public void onError(@NonNull Throwable e) {

                    }
                });

    }
}