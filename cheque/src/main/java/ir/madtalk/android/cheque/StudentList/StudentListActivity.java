package ir.madtalk.android.cheque.StudentList;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.os.Bundle;
import android.view.View;

import ir.madtalk.android.cheque.R;
import ir.madtalk.android.cheque.databinding.ActivityStudentListBinding;

public class StudentListActivity extends AppCompatActivity {

    ActivityStudentListBinding binding;
    StudentAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_student_list);

        adapter = new StudentAdapter(this);

        binding.toolbar.attachBtn.setVisibility(View.INVISIBLE);
        binding.toolbar.sendBtn.setVisibility(View.INVISIBLE);

        binding.toolbar.nameTv.setText("جست و جوی دانش آموزان");

        binding.toolbar.backBtn.setOnClickListener(view -> {
            onBackPressed();
        });

        StudentPresenter.getInstance().search_student(this);

        //StudentPresenter.getInstance().add_item(this);
    }
}