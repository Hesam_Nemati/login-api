package ir.madtalk.android.cheque.Cheque;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import ir.madtalk.android.cheque.NewCheque.NewChequeActivity;
import ir.madtalk.android.cheque.R;
import ir.madtalk.android.cheque.databinding.ActivityChequeListBinding;

public class ChequeListActivity extends AppCompatActivity {

    ActivityChequeListBinding binding;
    ChequeAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_cheque_list);
        adapter = new ChequeAdapter(this);

//        binding.chequeRecycler.scrollToPosition(adapter.versionModels.getValue().size()-1);

        binding.toolbar.attachBtn.setVisibility(View.INVISIBLE);
        binding.toolbar.sendBtn.setVisibility(View.INVISIBLE);
        binding.toolbar.nameTv.setText("لیست چک ها");
        binding.newChequeBtn.setVisibility(View.VISIBLE);



        ChequePresenter.getInstance().get_list(this);

        binding.chequeRecycler.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
//                Log.e("TAG", "onScrollStateChanged: " + newState);

                if (newState == 0) {
                    binding.newChequeBtn.setVisibility(View.VISIBLE);
                } else {
                    binding.newChequeBtn.setVisibility(View.INVISIBLE);
                }

            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
//                Log.e("Scroll: ", "onScrolled: " + dx + " / " + dy);
//                binding.newChequeBtn.setVisibility(View.INVISIBLE);
            }
        });

        binding.newChequeBtn.setOnClickListener(view -> {
            startActivity(new Intent(this, NewChequeActivity.class));
        });


    }

}