package ir.madtalk.android.cheque.Dynamic;

import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import com.google.android.material.appbar.MaterialToolbar;
import com.google.android.material.imageview.ShapeableImageView;
import com.google.android.material.progressindicator.CircularProgressIndicator;
import com.google.android.material.textview.MaterialTextView;

import ir.madtalk.android.cheque.R;

public class SingleChequeActivity extends AppCompatActivity {

    private static final String TAG = "SingleChequeActivity";

    private static final int MATCH_PARENT = -1;
    private static final int WRAP_CONTENT = -2;
    private static final int DEFAULT_MARGIN = 20;

    String name, reason, pay_to, amount, date_time, status;
    int id;

    boolean isLoaded = false;

    LinearLayout outerLinear;
    LinearLayout innerLinear;
    MaterialTextView nameTv;
    CircularProgressIndicator progressIndicator;
    MaterialToolbar toolbar;
    CardView cardView;
    RelativeLayout mainRelative;
    RelativeLayout.LayoutParams cardParams;
    ShapeableImageView statusImg;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState == null) {
            Bundle bundle = getIntent().getExtras();

            if (bundle != null) {
                if (bundle.containsKey("cheque_id")) {
                    id = bundle.getInt("cheque_id");
                }
            }
        }

        outerLinear = new LinearLayout(this);
        innerLinear = new LinearLayout(this);
        nameTv = new MaterialTextView(this);
        progressIndicator = new CircularProgressIndicator(this);
        toolbar = new MaterialToolbar(this);
        mainRelative = new RelativeLayout(this);
        statusImg = new ShapeableImageView(this);

        cardParams = new RelativeLayout.LayoutParams(MATCH_PARENT, WRAP_CONTENT);

        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(MATCH_PARENT, MATCH_PARENT);
        mainRelative.setLayoutParams(layoutParams);
        mainRelative.setBackgroundColor(getResources().getColor(R.color.white_light));

        SingleChequePresenter.getInstance().init_layout(this, DEFAULT_MARGIN);
        cardView = new CardView(this);
        cardView.setLayoutParams(cardParams);
        cardParams.addRule(RelativeLayout.BELOW, toolbar.getId());
        cardParams.setMargins(DEFAULT_MARGIN, DEFAULT_MARGIN, DEFAULT_MARGIN, DEFAULT_MARGIN);
        GradientDrawable shape = new GradientDrawable();
        shape.setCornerRadius(DEFAULT_MARGIN);
        cardView.setBackground(shape);
        cardView.setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
        cardView.setVisibility(View.INVISIBLE);



        ViewGroup.LayoutParams outerParams = new RelativeLayout.LayoutParams(MATCH_PARENT, MATCH_PARENT);
        outerLinear.setOrientation(LinearLayout.HORIZONTAL);
        outerLinear.setLayoutParams(outerParams);
        outerLinear.setId(View.generateViewId());

        LinearLayout.LayoutParams imgParams = new LinearLayout.LayoutParams(20, MATCH_PARENT);
        statusImg.setLayoutParams(imgParams);
        statusImg.setId(View.generateViewId());

        ViewGroup.LayoutParams linearParams = new ViewGroup.LayoutParams(MATCH_PARENT, MATCH_PARENT);
        innerLinear.setLayoutParams(linearParams);
        innerLinear.setOrientation(LinearLayout.VERTICAL);
        innerLinear.setBackgroundColor(getResources().getColor(R.color.white));

        LinearLayout.LayoutParams textParams = new LinearLayout.LayoutParams(MATCH_PARENT, WRAP_CONTENT);
        textParams.setMargins(DEFAULT_MARGIN, DEFAULT_MARGIN, DEFAULT_MARGIN, DEFAULT_MARGIN);
        nameTv.setLayoutParams(textParams);
        nameTv.setText(name);
        nameTv.setGravity(LinearLayout.FOCUS_RIGHT);
        nameTv.setTextColor(getResources().getColor(R.color.black_light));
        nameTv.setTextSize(17);
        cardView.setId(View.generateViewId());

        outerLinear.addView(statusImg);
        outerLinear.addView(innerLinear);
        cardView.addView(outerLinear);


        RelativeLayout.LayoutParams progressParams = new RelativeLayout.LayoutParams(WRAP_CONTENT, WRAP_CONTENT);
        progressParams.addRule(RelativeLayout.CENTER_IN_PARENT);
        progressIndicator.setLayoutParams(progressParams);
        progressIndicator.setIndeterminate(true);
        progressIndicator.setIndicatorColor(getResources().getColor(R.color.colorPrimaryDark));
        progressIndicator.setVisibility(View.VISIBLE);

        mainRelative.addView(progressIndicator);
        mainRelative.addView(cardView);

        setContentView(mainRelative);

    }


}
