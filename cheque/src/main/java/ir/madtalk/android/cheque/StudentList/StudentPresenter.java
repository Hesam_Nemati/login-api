package ir.madtalk.android.cheque.StudentList;

import android.text.Editable;
import android.text.TextWatcher;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;

import java.util.ArrayList;

import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import ir.madtalk.android.cheque.G;
import ir.madtalk.android.cheque.module.SearchAccount;
import ir.madtalk.android.cheque.module.Student_obj;
import ir.madtalk.android.cheque.netWork.NetworkLayer;

public class StudentPresenter {

    private static StudentPresenter presenter;

    public static StudentPresenter getInstance() {
        if (presenter == null)
            presenter = new StudentPresenter();
        return presenter;
    }

//    void add_item(StudentListActivity activity){
//        activity.adapter.versionModels.getValue().add(new Student_obj());
//        activity.adapter.versionModels.getValue().add(new Student_obj());
//        activity.adapter.versionModels.getValue().add(new Student_obj());
//        activity.adapter.versionModels.getValue().add(new Student_obj());
//        activity.adapter.versionModels.getValue().add(new Student_obj());
//        activity.adapter.versionModels.getValue().add(new Student_obj());
//        activity.adapter.versionModels.getValue().add(new Student_obj());
//        activity.adapter.versionModels.getValue().add(new Student_obj());
//
//        activity.binding.studentRecycler.setLayoutManager(new LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false));
//        activity.binding.studentRecycler.setAdapter(activity.adapter);
//    }

    public void search_student(StudentListActivity activity){

        activity.binding.searchEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {


                if (activity.binding.searchEt.getText().length() > 0) {

                    NetworkLayer.getInstance().search_account(G.getToken(), "student", String.valueOf(charSequence))
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribeOn(Schedulers.io())
                            .subscribe(new SingleObserver<ArrayList<SearchAccount>>() {
                                @Override
                                public void onSubscribe(@NonNull Disposable d) {

                                }

                                @Override
                                public void onSuccess(@NonNull ArrayList<SearchAccount> searchAccounts) {
                                    activity.binding.studentRecycler.setLayoutManager(new LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false));
                                    activity.binding.studentRecycler.setAdapter(activity.adapter);
                                    activity.adapter.versionModels.accept(searchAccounts);
                                }



                                @Override
                                public void onError(@NonNull Throwable e) {

                                }
                            });



                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

    }

}