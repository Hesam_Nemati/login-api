package ir.madtalk.android.cheque.StudentList;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.recyclerview.widget.RecyclerView;

import com.jakewharton.rxrelay2.BehaviorRelay;

import java.util.ArrayList;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import ir.madtalk.android.cheque.R;
import ir.madtalk.android.cheque.SingleCheque.SingleChequeActivity;
import ir.madtalk.android.cheque.databinding.ChequeItemBaseBinding;
import ir.madtalk.android.cheque.databinding.StudentBaseItemBinding;
import ir.madtalk.android.cheque.module.ChqueResult;
import ir.madtalk.android.cheque.module.SearchAccount;
import ir.madtalk.android.cheque.module.Student_obj;


public class StudentAdapter extends RecyclerView.Adapter<StudentAdapter.VersionViewHolder> {
    StudentListActivity context;
    CompositeDisposable bag = new CompositeDisposable();
    private int width;
    BehaviorRelay<ArrayList<SearchAccount>> versionModels = BehaviorRelay.createDefault(new ArrayList<>());

    {
        versionModels.observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<ArrayList<SearchAccount>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        bag.add(d);
                    }

                    @Override
                    public void onNext(ArrayList<SearchAccount> students) {

                        notifyDataSetChanged();
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }


    public StudentAdapter(StudentListActivity context) {
        this.context = context;

    }


    @Override
    public VersionViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.student_base_item, parent, false);
        VersionViewHolder viewHolder = new VersionViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final VersionViewHolder holder, final int position) {


        SearchAccount student_obj = versionModels.getValue().get(position);
        holder.binding.studentNameTv.setText(versionModels.getValue().get(position).getFirst_name() + " " + versionModels.getValue().get(position).getLast_name());




    }


    @Override
    public int getItemCount() {
        return versionModels == null ? 0 : versionModels.getValue().size();
    }

    class VersionViewHolder extends RecyclerView.ViewHolder {

        StudentBaseItemBinding binding;


        public VersionViewHolder(View itemView) {
            super(itemView);
            binding = StudentBaseItemBinding.bind(itemView);

            binding.studentCard.setOnClickListener(view -> {
                intent();
            });


        }


        public void intent(){
            String fullname = versionModels.getValue().get(getAdapterPosition()).getFirst_name()
                    + " "
                    +versionModels.getValue().get(getAdapterPosition()).getLast_name();

            Intent intent = new Intent(context, SingleChequeActivity.class);
            intent.putExtra("student_id", versionModels.getValue().get(getAdapterPosition()).getId());
            intent.putExtra("student_name", fullname);
            context.setResult(Activity.RESULT_OK, intent);
            context.finish();
        }

    }


}
