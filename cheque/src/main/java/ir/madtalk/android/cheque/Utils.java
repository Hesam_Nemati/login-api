package ir.madtalk.android.cheque;

import android.widget.EditText;

public class Utils {
    private static Utils utils = new Utils();

    public static Utils getInstance() {
        return utils;
    }


    public static boolean is_empty(EditText... ets){
        for (int i = 0; i <ets.length ; i++) {
            if (is_empty(ets[i])){
                return true;
            }
        }

        return false;
    }
}
