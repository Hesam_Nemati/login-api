package ir.madtalk.android.cheque.Dynamic;

import static android.view.ViewGroup.LayoutParams.MATCH_PARENT;
import static android.view.ViewGroup.LayoutParams.WRAP_CONTENT;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.graphics.Path;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.material.appbar.MaterialToolbar;
import com.google.android.material.textview.MaterialTextView;

import ir.madtalk.android.cheque.R;

public class Views {

    private static Views views;

    public static Views getInstance() {
        if (views == null)
            views = new Views();

        return views;
    }

    /***
     * This Method returns a simple Horizontal Linear Layout contains two TextViews:
     * 1. for title
     * 2. for value (taken from API)
     *
     * @param context - context is needed to define views
     * @param title - static title of first textView
     * @param value - taken from API
     * @param margin - margin of textViews
     * @return returns LinearLayout
     */
    public LinearLayout sc_items_ll(Context context, String title, String value, int margin) {

        LinearLayout linearLayout = new LinearLayout(context);
        ViewGroup.LayoutParams linearParams = new ViewGroup.LayoutParams(MATCH_PARENT, WRAP_CONTENT);
        linearLayout.setLayoutParams(linearParams);
        linearLayout.setOrientation(LinearLayout.HORIZONTAL);
        linearLayout.setGravity(View.SCROLL_INDICATOR_RIGHT);
        linearLayout.setHorizontalGravity(View.FOCUS_RIGHT);
        linearLayout.setWeightSum(2);

        LinearLayout.LayoutParams tvParam = new LinearLayout.LayoutParams(0, WRAP_CONTENT);
        tvParam.weight = 1;
        tvParam.setMargins(margin, margin, margin, margin);

        MaterialTextView titleTv = new MaterialTextView(context);
        titleTv.setLayoutParams(tvParam);
        titleTv.setGravity(View.SCROLL_INDICATOR_RIGHT);
        titleTv.setText(title);

        MaterialTextView valueTv = new MaterialTextView(context);
        valueTv.setLayoutParams(tvParam);
        valueTv.setText(value);
        valueTv.setTextSize(17);
        valueTv.setTextColor(context.getResources().getColor(R.color.black_light));
        valueTv.setSingleLine(true);

        linearLayout.addView(titleTv, 0);
        linearLayout.addView(valueTv, 1);

        return linearLayout;
    }

    @SuppressLint("UseCompatLoadingForDrawables")
    public void simple_toolbar(Activity context, MaterialToolbar toolbar, String title, int margin) {

//        toolbar = new MaterialToolbar(context);
        RelativeLayout.LayoutParams toolbarParams = new RelativeLayout.LayoutParams(MATCH_PARENT, WRAP_CONTENT);
        toolbarParams.addRule(RelativeLayout.ALIGN_PARENT_TOP);
        toolbarParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
        toolbarParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
        toolbar.setLayoutParams(toolbarParams);
        toolbar.setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
        toolbar.setBackgroundColor(context.getResources().getColor(R.color.white));

        RelativeLayout rltv = new RelativeLayout(context);
        RelativeLayout.LayoutParams rltvParams = new RelativeLayout.LayoutParams(MATCH_PARENT, WRAP_CONTENT);
        rltv.setLayoutParams(rltvParams);
        rltv.setBackgroundColor(context.getResources().getColor(R.color.white));

        ImageButton backBtn = new ImageButton(context);
        RelativeLayout.LayoutParams btnParams = new RelativeLayout.LayoutParams(WRAP_CONTENT, WRAP_CONTENT);
        backBtn.setId(View.generateViewId());
        backBtn.setLayoutParams(btnParams);
        backBtn.setBackground(context.getResources().getDrawable(R.drawable.ic_action_back_black));

        TextView titleTv = new TextView(context);
        RelativeLayout.LayoutParams tvParams = new RelativeLayout.LayoutParams(WRAP_CONTENT, WRAP_CONTENT);
        titleTv.setId(View.generateViewId());
        titleTv.setLayoutParams(tvParams);
        titleTv.setText(title);
        titleTv.setTextSize(20);
        titleTv.setTextColor(context.getResources().getColor(R.color.black_light));

        btnParams.setMargins(margin, margin, margin, margin);
        btnParams.addRule(RelativeLayout.ALIGN_PARENT_START);
        btnParams.addRule(RelativeLayout.CENTER_VERTICAL);

        backBtn.setOnClickListener(view -> {
            context.onBackPressed();
        });

        rltv.addView(backBtn);

        tvParams.setMargins(margin, margin, margin, margin);
        tvParams.addRule(RelativeLayout.LEFT_OF, backBtn.getId());
        rltv.addView(titleTv);

        toolbar.addView(rltv);


//        return toolbar;
    }


}
