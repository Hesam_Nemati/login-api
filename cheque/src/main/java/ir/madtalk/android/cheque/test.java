package ir.madtalk.android.cheque;

import static android.os.Build.VERSION_CODES.P;
import static android.view.ViewGroup.LayoutParams.MATCH_PARENT;
import static android.view.ViewGroup.LayoutParams.WRAP_CONTENT;
import static android.widget.RelativeLayout.ALIGN_BOTTOM;
import static android.widget.RelativeLayout.ALIGN_LEFT;
import static android.widget.RelativeLayout.ALIGN_PARENT_LEFT;
import static android.widget.RelativeLayout.ALIGN_PARENT_RIGHT;
import static android.widget.RelativeLayout.ALIGN_RIGHT;
import static android.widget.RelativeLayout.ALIGN_TOP;
import static android.widget.RelativeLayout.BELOW;
import static android.widget.RelativeLayout.CENTER_IN_PARENT;
import static android.widget.RelativeLayout.CENTER_VERTICAL;
import static android.widget.RelativeLayout.LEFT_OF;
import static android.widget.RelativeLayout.RIGHT_OF;

import android.content.Intent;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.textview.MaterialTextView;

import de.hdodenhof.circleimageview.CircleImageView;


public class test extends AppCompatActivity {

    private static final int MATCH_PARENT = -1;
    private static final int WRAP_CONTENT = -2;


    RelativeLayout relativeLayout;
    RelativeLayout progressRelative;
    ProgressBar progressBar;
    CircleImageView circleImageView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        relativeLayout = new RelativeLayout(this);
        progressRelative = new RelativeLayout(this);
        progressBar = new ProgressBar(this);
        circleImageView = new CircleImageView(this);


        RelativeLayout.LayoutParams relative_param = new RelativeLayout.LayoutParams(MATCH_PARENT, MATCH_PARENT);
        relativeLayout.setLayoutParams(relative_param);


        RelativeLayout.LayoutParams progressRelativeParam = new RelativeLayout.LayoutParams(MATCH_PARENT, MATCH_PARENT);
        progressRelative.setLayoutParams(progressRelativeParam);

        RelativeLayout.LayoutParams circleImageViewParam = new RelativeLayout.LayoutParams(WRAP_CONTENT, WRAP_CONTENT);
        circleImageView.setLayoutParams(circleImageViewParam);
        circleImageView.setImageResource(R.drawable.ic_launcher_background);
        circleImageView.setId(View.generateViewId());
        circleImageViewParam.addRule(CENTER_IN_PARENT);


        RelativeLayout.LayoutParams progressParam = new RelativeLayout.LayoutParams(WRAP_CONTENT, WRAP_CONTENT);
        progressBar.setLayoutParams(progressParam);
        progressParam.addRule(CENTER_IN_PARENT);
        progressParam.addRule(ALIGN_LEFT, circleImageView.getId());
        progressParam.addRule(ALIGN_RIGHT, circleImageView.getId());
        progressParam.addRule(ALIGN_TOP, circleImageView.getId());
        progressParam.addRule(ALIGN_BOTTOM, circleImageView.getId());
        progressParam.setMargins(-41, -39, -38, -40);
        progressParam.setMarginEnd(-40);
        progressParam.setMarginStart(-40);


        setContentView(relativeLayout);
        relativeLayout.addView(progressRelative, progressRelativeParam);
        progressRelative.addView(progressBar, progressParam);
        progressRelative.addView(circleImageView, circleImageViewParam);


    }
}
