package ir.madtalk.android.cheque.module;

import com.google.gson.annotations.SerializedName;

public class SearchAccount {

    @SerializedName("first_name")
    private String first_name;

    @SerializedName("id")
    private int id;

    @SerializedName("last_name")
    private String last_name;

    @SerializedName("user")
    private int user;

    public String getFirst_name() {
        return first_name;
    }

    public int getId() {
        return id;
    }

    public String getLast_name() {
        return last_name;
    }

    public int getUser() {
        return user;
    }
}
