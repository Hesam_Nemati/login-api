package ir.madtalk.android.cheque.Dynamic;

import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import ir.madtalk.android.cheque.G;
import ir.madtalk.android.cheque.module.ChequeList;
import ir.madtalk.android.cheque.netWork.NetworkLayer;

public class ChequeListPresenter {

    private static ChequeListPresenter presenter;

    public static ChequeListPresenter getInstance() {
        if (presenter == null)
            presenter = new ChequeListPresenter();
        return presenter;
    }

    public void get_list(ChequeListActivity activity){
        NetworkLayer.getInstance().get_cheque_list(G.getToken())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new SingleObserver<ChequeList>() {

                    @Override
                    public void onSubscribe(@NonNull Disposable d) {
                    }

                    @Override
                    public void onSuccess(@NonNull ChequeList chequeList) {

                        activity.chequeRecycler.setLayoutManager(new LinearLayoutManager(activity, RecyclerView.VERTICAL, true));
                        activity.adapter.versionModels.accept(chequeList.getChque_result());
                        activity.chequeRecycler.setAdapter(activity.adapter);

                        activity.progressIndicator.setVisibility(View.INVISIBLE);
                        activity.addChequeBtn.setVisibility(View.VISIBLE);
                        activity.chequeRecycler.setVisibility(View.VISIBLE);

                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        e.printStackTrace();
                    }
                });

    }


}
