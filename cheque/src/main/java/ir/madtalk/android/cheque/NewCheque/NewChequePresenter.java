package ir.madtalk.android.cheque.NewCheque;

import android.widget.Toast;

import androidx.annotation.NonNull;

import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import ir.madtalk.android.cheque.G;
import ir.madtalk.android.cheque.module.CreatCheque;
import ir.madtalk.android.cheque.netWork.NetworkLayer;

public class NewChequePresenter {

    private static final NewChequePresenter ourInstance = new NewChequePresenter();


    public static NewChequePresenter getInstance() {
        return ourInstance;
    }

    public NewChequePresenter() {
    }

    public void creat_new_cheque(NewChequeActivity activity , String token , int student_id , String for_what , String payـto , String exporter ,
                String bank , int amount , int state , boolean sayad){

        NetworkLayer.getInstance().creat_cheque(token , student_id , for_what , payـto , exporter , bank , amount ,state , sayad)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new SingleObserver<CreatCheque>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {

                    }

                    @Override
                    public void onSuccess(@NonNull CreatCheque creatCheque) {
                        Toast.makeText(activity, "Added", Toast.LENGTH_SHORT).show();
                        activity.finish();
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        Toast.makeText(activity, "Error", Toast.LENGTH_SHORT).show();
                    }
                });
    }

}
